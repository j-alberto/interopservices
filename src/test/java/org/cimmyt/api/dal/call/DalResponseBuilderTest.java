package org.cimmyt.api.dal.call;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;

import org.assertj.core.api.Condition;
import org.cimmyt.api.dal.to.DalEnum;
import org.cimmyt.test.util.TestResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.data.domain.Page;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@RunWith(BlockJUnit4ClassRunner.class)

public class DalResponseBuilderTest {

	private DalEnum testDalEnum = DalEnum.Trait;
	private ObjectMapper jsonMapper = new ObjectMapper();
	private XmlMapper xmlMapper = new XmlMapper();
	
	@Mock
	private Page<String> mockPage;
	
	@Before
	public void setBeforeClass(){
		MockitoAnnotations.initMocks(this);

		when(mockPage.getNumber()).thenReturn(0);
		when(mockPage.getNumberOfElements()).thenReturn(10);
		when(mockPage.getSize()).thenReturn(10);
		when(mockPage.getTotalElements()).thenReturn(50L);
		when(mockPage.getTotalPages()).thenReturn(5);
		
	}

	@Test
	public void shouldContainRecordMeta() throws Exception {
		DALResponse<String> actual = DalResponseBuilder.forData("", testDalEnum)
		.build();
	
		assertThat(actual.getComponents())
			.containsKey("RecordMeta")
			.doesNotContainKey("Pagination")
			.doesNotContainKey("WriteToken");
		
	 	Condition<Object> tagTrait = new Condition<Object>() {
		   public boolean matches(Object value) {
		     return ((RecordMeta)value).getTagName().equals(testDalEnum);
		   }
		 };
	
		assertThat(actual.getComponents().get("RecordMeta"))
			.asList().areExactly(1, tagTrait );
	}
	
	@Test
	public void shouldContainPayload() {
		DALResponse<String> actual = DalResponseBuilder
			.forData("test payload", testDalEnum).build();

		assertThat(actual.getComponents())
			.containsKey(testDalEnum.toString())
			.doesNotContainKey("Pagination")
			.doesNotContainKey("WriteToken");
		
		assertThat(actual.getComponents().get(testDalEnum.toString()))
			.isEqualTo("test payload");
	}
	
	@Test
	public void shouldContainPagination() {
		DALResponse<String> actual = DalResponseBuilder
			.forData("test payload", testDalEnum)
			.withPagination(mockPage).build();

		assertThat(actual.getComponents())
			.containsKey(testDalEnum.toString())
			.containsKey("Pagination")
			.doesNotContainKey("WriteToken");
		
		Pagination page = (Pagination)((List<?>)actual.getComponents().get("Pagination")).get(0);
		assertThat(page)
			.extracting("numPerPage","page","numOfRecords","numOfPages")
			.contains(10,0,50L,5);
	}
	
	@Test
	public void shouldContainWriteToken() {
		DALResponse<String> actual = DalResponseBuilder
			.forData("test payload", testDalEnum,"token123")
			.build();

		assertThat(actual.getComponents())
			.containsKey(testDalEnum.toString())
			.containsKey("WriteToken")
			.doesNotContainKey("Pagination");
		
		WriteToken token = (WriteToken)((List<?>)actual.getComponents().get("WriteToken")).get(0);
		assertThat(token)
			.extracting("value")
			.contains("token123");
	}
	
	@Test
	public void willBeValidFormat() throws Exception {
		DALResponse<String> response = DalResponseBuilder
			.forData("test payload", testDalEnum)
			.withPagination(mockPage).build();
		
		String actual = jsonMapper.writeValueAsString(response);
		String expected = TestResource.getJSONFile("dalResponse1");
		
		JSONAssert.assertEquals(expected, actual, true);

		actual = xmlMapper.writeValueAsString(response);
		expected = TestResource.getXMLFile("dalResponse1");
		
		assertThat(actual).isEqualTo(expected);

	}

	@Test
	public void willBeValidTokenFormat() throws Exception {
		DALResponse<String> response = DalResponseBuilder
			.forData("test payload", testDalEnum, "token123")
			.withNoPagination().build();
		
		String actual = jsonMapper.writeValueAsString(response);
		String expected = TestResource.getJSONFile("dalResponse2");
		
		JSONAssert.assertEquals(expected, actual, true);

		actual = xmlMapper.writeValueAsString(response);
		expected = TestResource.getXMLFile("dalResponse2");

		assertThat(actual).isEqualTo(expected);
}
	
	@Test
	public void willBeValidInfoFormat() throws Exception {
		DALInfoResponse response = DalResponseBuilder
			.forInfo("aKey", "aValue")
			.and("aKey2", "aValue2").build();
		
		String actual = jsonMapper.writeValueAsString(response);
		String expected = TestResource.getJSONFile("dalResponse3");
		
		JSONAssert.assertEquals(expected, actual, true);

		actual = xmlMapper.writeValueAsString(response);
		expected = TestResource.getXMLFile("dalResponse3");

		assertThat(actual).isEqualTo(expected);

	}

}
