package org.cimmyt.api.dal.conversion;

import static org.assertj.core.api.Assertions.assertThat;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.dal.to.Trial;
import org.cimmyt.model.Study;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class StudyToDalTrialConverterTest {

	private StudyToDalTrialConverter testObject = new StudyToDalTrialConverter();
	private Study tester;
	ApiTO<Study> result;
	
	@Test
	public void shouldBeDalConverter(){
		ApiConversion[] ann = testObject.getClass().getAnnotationsByType(ApiConversion.class);

		assertThat(ann).isNotNull();
		assertThat(ann[0].value()).isEqualTo(ApiFormat.DAL);
		
	}
	
	@Test
	public void shouldConvert() {
		tester = new Study();
		tester.setName("my name");
		tester.setTitle("my title");
		tester.setYear(2017);
		tester.setId(123);
		result = testObject.convert(tester);
		
		assertThat(result)
			.isInstanceOf(Trial.class);
		
		assertThat(result)
			.extracting("trialName","trialTitle","trialYear")
			.contains("my name","my title",2017);
		assertThat(result)
			.extracting("extra").extracting("update")
			.contains("update/trial/123");
	}
	
	@Test
	public void shouldConvertWithOnlyId() {
		tester = new Study();
		tester.setId(999);
		result = testObject.convert(tester);
		
		assertThat(result)
			.extracting("trialName","trialTitle","trialYear")
			.contains(null,null,null);
		assertThat(result)
			.extracting("extra").extracting("update")
			.contains("update/trial/999");
	}
}
