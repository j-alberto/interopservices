package org.cimmyt.api.dal.resource;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.to.Trial;
import org.cimmyt.config.InteropApplication;
import org.cimmyt.model.Study;
import org.cimmyt.service.StudyService;
import org.cimmyt.test.util.TestResource;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InteropApplication.class)
@AutoConfigureMockMvc
public class DalStudyResourceTest {

	@Autowired
	private MockMvc mvc;
	@MockBean
	StudyService studyService;
	@MockBean
	ApiConversionService<Study> studyConversionService;

	@Autowired
	private TestResource resource;

	private static Page<ApiTO<Study>> dalTestStudies;

	@BeforeClass
	public static void beforeClass() {
		Trial t1 = new Trial();
		Trial t2 = new Trial();
		t1.setTrialName("a name1");
		//t1.setTrialTitle("a title");
		//t1.setTrialYear(1999);
		t1.setUpdateLink("/update/link/1");
		t2.setTrialName("a name2");
		//t2.setTrialTitle("a title 2");
		//t2.setTrialYear(2000);
		t2.addExtra("update", "/update/link/2");
		dalTestStudies = new PageImpl<>(Arrays.asList(t1, t2), new PageRequest(0, 2), 2);

	}

	@Mock
	Page<Study> studyPageStub;

	@Before
	public void before() {
		given(studyService.getStudies(any())).willReturn(studyPageStub);
	}

	@Test
	public void shouldReturnStudies() throws Exception {
		given(studyConversionService.convert(studyPageStub)).willReturn(dalTestStudies);

		mvc.perform(get("/dal/list/trial/2/page/1").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(resource.getJsonFile("dalTrials")));

	}
}
