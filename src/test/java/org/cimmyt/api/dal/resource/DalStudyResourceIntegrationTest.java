package org.cimmyt.api.dal.resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.cimmyt.config.InteropApplication;
import org.cimmyt.test.util.TestResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InteropApplication.class)
@AutoConfigureMockMvc
public class DalStudyResourceIntegrationTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private TestResource resource;

	@Test
	public void shouldReturnStudiesWithDefaultPagination() throws Exception {
		mvc.perform(get("/dal/list/trial/20/page/2").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(resource.getJsonFile("dalTrials-Integration")));

	}
	
}
