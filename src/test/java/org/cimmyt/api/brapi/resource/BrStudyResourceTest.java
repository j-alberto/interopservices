package org.cimmyt.api.brapi.resource;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.brapi.to.BrStudyTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.config.InteropApplication;
import org.cimmyt.model.Study;
import org.cimmyt.service.StudyService;
import org.cimmyt.test.util.TestResource;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InteropApplication.class)
@AutoConfigureMockMvc
public class BrStudyResourceTest {

	@Autowired
	private MockMvc mvc;
	@MockBean
	StudyService studyService;
	@MockBean
	ApiConversionService<Study> studyConversionService;

	@Autowired
	private TestResource resource;

	private static Page<ApiTO<Study>> brapiTestStudies;

	@BeforeClass
	public static void beforeClass() {
		BrStudyTO s1 = new BrStudyTO();
		BrStudyTO s2 = new BrStudyTO();

		s1.setaYear(1999);
		s1.setName("a name1");
		s1.setTitle("a title");
		s2.setaYear(2000);
		s2.setName("a name2");
		s2.setTitle("a title 2");
		brapiTestStudies = new PageImpl<>(Arrays.asList(s1, s2), new PageRequest(0, 2), 2);
	}

	@Mock
	Page<Study> studyPageStub;

	@Before
	public void before() {
		given(studyService.getStudies(any())).willReturn(studyPageStub);
	}

	@Test
	public void shouldReturnStudies() throws Exception {
		given(studyConversionService.convert(studyPageStub)).willReturn(brapiTestStudies);

		mvc.perform(get("/brapi/study").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().json(resource.getJsonFile("brapiStudies")));

	}

}
