package org.cimmyt.api.brapi.to;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.setAllowExtractingPrivateFields;

import org.cimmyt.api.JavaBeanTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;


@RunWith(BlockJUnit4ClassRunner.class)
public class BrStudyTOTest implements JavaBeanTest{

	@Test
	@Override
	public void hasValidMutators() {
		setAllowExtractingPrivateFields(false);
		
		BrStudyTO testObject = new BrStudyTO();
		testObject.setaYear(2017);
		testObject.setName("a name");
		testObject.setTitle("some title");
		
		assertThat(testObject)
			.extracting("aYear","name","title")
			.contains(2017,"a name","some title");
	}

}
