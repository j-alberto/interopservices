package org.cimmyt.service;

import org.cimmyt.model.Study;
import org.cimmyt.repository.StudyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
public class StudyServiceImpl implements StudyService {

	private StudyRepository studyRepository;

	@Autowired
	public StudyServiceImpl(StudyRepository studyRepository) {
		super();
		this.studyRepository = studyRepository;
	}
	
	@Override
	public Page<Study> getStudies(Pageable page){
		Example<Study> example = Example.of(new Study(34055));
		//Example<Study> example = Example.of(new Study("iwis sync"));
		
		
		return studyRepository.findAll(example,page);
		
		//return studyRepository.findAll(page);
	}
}
