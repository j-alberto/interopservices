package org.cimmyt.service;

import java.util.List;

import org.cimmyt.model.B4RVariables;
import org.cimmyt.model.ScaleValue;
import org.cimmyt.repository.StudyTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author RHTOLEDO
 *
 */

@Service
@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
public class ScaleValueServiceImpl implements ScaleValueService{

	
	private StudyTypeRepository studyTypeRepository;
	
	@Autowired
	public ScaleValueServiceImpl(StudyTypeRepository studyTypeRepository){
		super();
		this.studyTypeRepository = studyTypeRepository;
	}
	
	/**
	 *  The study types are inside the table SCALES_VALUES
	 *  with a specific scaleId. See {@link B4RVariables}
	 */
	@Override
	public List<ScaleValue> getStudyType() {
		
		Integer scaleId = B4RVariables.STUDY_TYPE;
		
		Example<ScaleValue> example = Example.of(new ScaleValue(scaleId));
		return studyTypeRepository.findAll(example);
	}

}
