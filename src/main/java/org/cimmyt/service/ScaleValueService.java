package org.cimmyt.service;

import java.util.List;

import org.cimmyt.model.ScaleValue;

/**
 * Logic to manage {@link ScaleValue}
 * @author RHTOLEDO
 *
 */

public interface ScaleValueService {
	
	/**
	 * Refer to {@link B4RValues} to see the scale_id to Study Type
	 * DAL Implementation
	 * @return
	 */
	List<ScaleValue> getStudyType();

}
