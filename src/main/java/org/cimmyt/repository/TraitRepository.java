package org.cimmyt.repository;

import org.springframework.stereotype.Repository;
import org.cimmyt.model.Variable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * @author RHTOLEDO
 *
 */

@Repository
public interface TraitRepository extends JpaRepository<Variable,Integer>{

}
