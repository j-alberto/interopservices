package org.cimmyt.repository;

import org.cimmyt.model.Plot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author RHTOLEDO
 *
 */
@Repository
public interface PlotRepository extends JpaRepository<Plot,Integer>{

}
