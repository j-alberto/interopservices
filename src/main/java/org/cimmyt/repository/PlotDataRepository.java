package org.cimmyt.repository;

import org.cimmyt.model.PlotData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author RHTOLEDO
 *
 */
@Repository
public interface PlotDataRepository extends JpaRepository<PlotData,Integer>{

}
