package org.cimmyt.config;

import org.springframework.data.domain.PageRequest;

/**
 * This class allow to start the page number {page} in the request with 1 instead of 0
 * http:/localhost/dal/.../list/{size}/page/{page}
 *  
 * @author RHTOLEDO
 *
 */

public class DalPageResquest extends PageRequest{
	

	private static final long serialVersionUID = 1L;

	public DalPageResquest(int page, int size){
		super(--page,size);
	}

}
