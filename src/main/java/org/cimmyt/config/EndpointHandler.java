package org.cimmyt.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cimmyt.api.ApiFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Identifies and sets the {@link EndpointFormat} properly for each request
 * @author jarojas
 *
 */
class EndpointHandler extends HandlerInterceptorAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(EndpointHandler.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		try {
			String api = request.getRequestURI().split("/", 3)[1].toUpperCase();
			
			LOG.info("Request received: {}", request.getRequestURI());
			LOG.info("Using transformation for api: {}", api);
			EndpointFormat.setCurrent(ApiFormat.valueOf(api));
		} catch (IllegalArgumentException ex) {
			LOG.warn("Cannot infer api from request, defaulting to: {}", ApiFormat.BRAPI);
			EndpointFormat.setCurrent(ApiFormat.BRAPI);
		}
		
		return super.preHandle(request, response, handler);
	}
}
