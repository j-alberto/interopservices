package org.cimmyt.config;


import java.util.List;

import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.conversion.ApiConversionServiceImpl;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.model.BreedingMethodVariable;
import org.cimmyt.model.Container;
import org.cimmyt.model.Place;
import org.cimmyt.model.Plot;
import org.cimmyt.model.PlotData;
import org.cimmyt.model.Program;
import org.cimmyt.model.RestOperations;
import org.cimmyt.model.ScaleValue;
import org.cimmyt.model.SearchInfo;
import org.cimmyt.model.SeedStorage;
import org.cimmyt.model.Study;
import org.cimmyt.model.Units;
import org.cimmyt.model.User;
import org.cimmyt.model.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for declaring concrete {@link ApiConversionService API conversion services} in the application
 * @author jarojas
 *
 */
@Configuration
public class ConversionsConfig {
	
	private static final Logger LOG = LoggerFactory.getLogger(ConversionsConfig.class);

	/**
	 * {@link ApiConversionService converter} Service Bean for Study entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<Study> studyConversionService(List<ApiConverter<Study>> stConverters){
		LOG.debug("Study converters registered: {}", stConverters.size());

		return new ApiConversionServiceImpl<>(stConverters);
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for Program entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<Program> programConversionService(List<ApiConverter<Program>> stConverters){
		LOG.debug("Program converters registered: {}", stConverters.size());

		return new ApiConversionServiceImpl<>(stConverters);
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for Place entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<Place> locationConversionService(List<ApiConverter<Place>> stConverters){
		LOG.debug("Location converters registered: {}", stConverters.size());

		return new ApiConversionServiceImpl<>(stConverters);
	}	
	
	/**
	 * {@link ApiConversionService converter} Service Bean for Plot entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<Plot> plotConversionService(List<ApiConverter<Plot>> stConverters){
		LOG.debug("Plot converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for ScaleValue entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<ScaleValue> scaleValueConversionService(List<ApiConverter<ScaleValue>> stConverters){
		LOG.debug("ScaleValue converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
	}	
	
	
	/**
	 * {@link ApiConversionService converter} Service Bean for Variable entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<Variable> plotVariableConversionService(List<ApiConverter<Variable>> stConverters){
		LOG.debug("PlotData converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for User entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<User> userConversionService(List<ApiConverter<User>> stConverters){
		LOG.debug("User converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for RestOperations entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<RestOperations> operationConversionService(List<ApiConverter<RestOperations>> stConverters){
		LOG.debug("RestOperations converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
	}	
	
	/**
	 * {@link ApiConversionService converter} Service Bean for Units entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<Units> unitsConversionService(List<ApiConverter<Units>> stConverters){
		LOG.debug("Unit converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
		
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for SearchInfo entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<SearchInfo> searchConversionService(List<ApiConverter<SearchInfo>> stConverters){
		LOG.debug("SearchInfo converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
		
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for SeedStorage entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<SeedStorage> seedStorageConversionService(List<ApiConverter<SeedStorage>> stConverters){
		LOG.debug("SeedStorage converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
		
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for Container entity
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<Container> containerConversionService(List<ApiConverter<Container>> stConverters){
		LOG.debug("Container converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
		
	}
	
	/**
	 * {@link ApiConversionService converter} Service Bean for PlotData entity
	 * @param srConverters
	 * @return
	 */
	@Bean
	ApiConversionService<PlotData> plotDataConversionService(List<ApiConverter<PlotData>> stConverters){
		LOG.debug("PlotData converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters); 
	}
	
	/**
	 * {@link ApiConversionService converter} Service
	 * @param stConverters
	 * @return
	 */
	@Bean
	ApiConversionService<BreedingMethodVariable> breedingMetConversionService(List<ApiConverter<BreedingMethodVariable>> stConverters){
		LOG.debug("Breeding Method converters registered: {}", stConverters.size());
		
		return new ApiConversionServiceImpl<>(stConverters);
		
	}
	
}
