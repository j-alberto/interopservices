package org.cimmyt.api.advice;

import javax.servlet.http.HttpServletRequest;

import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Handler for intercepting and processing (checked) exceptions thrown (and not managed) by DAL-controllers.
 * Specific subclasses of {@link Exception} can be grouped in different methods inside this class.
 * For example all Business Logic Exceptions can be returned with code 505, 
 * and all Validation Exceptions with code 400 <br/>
 * @author jarojas
 *
 */
@RestControllerAdvice(basePackages={"org.cimmyt.api.dal.resource"})
public class DalErrorHandlerAdvice {

	/**
	 * Method to catch all exceptions extending {@link Exception}. Responds with code 500.
	 * Further specializations may come along with new features.
	 * @param request originating the exception
	 * @param handler controlling given request
	 * @param exception to be processed
	 * @return a {@link DALResponse response} with http status code 500 (Internal Server Error)
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(code=HttpStatus.INTERNAL_SERVER_ERROR)
	public DALResponse<?> dispatchServerError(HttpServletRequest request, Object handler, Exception exception) {
		return DalResponseBuilder.forError(exception.getMessage());
	}
}
