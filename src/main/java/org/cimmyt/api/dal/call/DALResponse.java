/*
 * BMSAPI - BrAPI expansion
 * CIMMYT, 2016
 * License pending
 */
package org.cimmyt.api.dal.call;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cimmyt.api.ApiResponse;
import org.cimmyt.api.dal.resource.DalDoLoginResource;
import org.cimmyt.api.dal.to.DalEnum;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * Global format for any DAL call
 * @author jarojas
 *
 * @param <T> the payload type in this response
 */
//TODO reduce T to ApiTO
@JsonNaming(UpperCamelCaseStrategy.class)
public class DALResponse<T> implements ApiResponse<T>{
	
	private Map<String, Object> elements;
	private DalEnum entity;
	public DALResponse(DalEnum entity) {
		this.elements = new HashMap<>();
		this.entity = entity;
	}

	public DALResponse(Map<String, Object> elements, DalEnum entity) {
		super();
		this.elements = elements;
		this.entity = entity;
	}

	@JsonAnyGetter
	public Map<String, Object> getComponents() {
		return elements;
	}
	
	public void setPagination(List<Pagination> pagination){
		elements.put("Pagination", pagination);
	}
	public void setRecordMeta(List<RecordMeta> recordMeta){
		elements.put("RecordMeta", recordMeta);
	}
	
	/**
	 * Write token is used by the login functionality, see {@link DalDoLoginResource} 
	 * @param token
	 */
	public void setWriteToken(List<WriteToken> token){
	    elements.put("WriteToken", token);	
	}
	
	
	/**
	 * TODO: Remove this code !!!!
	 * @param label
	 * @param meta
	 */
	public void setOtherMeta(String label, T meta){
		elements.put(label, meta);
		
		Pagination pag = new Pagination();
		pag.setPage(1);
		pag.setNumOfPages(1);
		pag.setNumOfRecords(2);
		pag.setNumPerPage(2);
		
		elements.put("Pagination", Arrays.asList(pag));
	} 
	
	public void setPayload(T payload){
		elements.put(entity.toString(), payload);
	}
}
