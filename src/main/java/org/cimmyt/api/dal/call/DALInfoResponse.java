/*
 * BMSAPI - BrAPI expansion
 * CIMMYT, 2016
 * License pending
 */
package org.cimmyt.api.dal.call;

import java.util.HashMap;
import java.util.Map;

import org.cimmyt.api.ApiResponse;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * Format for special DAL calls with meta information like version, etc
 * @author jarojas
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
@JacksonXmlRootElement(localName="DATA")
public class DALInfoResponse implements ApiResponse<String>{

	@JacksonXmlProperty(isAttribute=true)
	private Map<String, String> info;
	
	public DALInfoResponse() {
		info = new HashMap<>();
	}

	public Map<String, String> getInfo() {
		return info;
	}

	public void setInfo(Map<String, String> info) {
		this.info = info;
	}
	
	public void put(String key, String value){
		info.put(key, value);
	}
}
