/*
 * BMSAPI - BrAPI expansion
 * CIMMYT, 2016
 * License pending
 */
package org.cimmyt.api.dal.call;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import org.cimmyt.api.dal.to.DalEnum;
import org.springframework.data.domain.Page;

/**
 * Singleton-fluent builder for responses that comply with DAL standards.
 * @author jarojas
 * @category utility
 */
//TODO create forPagedData. Remove withPagination in Response inner class
public final class DalResponseBuilder {
	
	private static DalResponseBuilder responseBuilder = new DalResponseBuilder();
	
	private DalResponseBuilder(){}

	/**
	 * Builder's entry point
	 * @param payload to add in the response
	 * @return intermediate {@link Response} for further configuration
	 */
	public static <T>Response<T> forData(T payload, DalEnum dalEntity){
		return responseBuilder.new Response<T>(payload, dalEntity);
	}
	
	/**
	 * Builder's entry point for login service
	 * @param payload
	 * @param dalEntity
	 * @param token
	 * @return
	 */
	public static <T>Response<T> forData(T payload, DalEnum dalEntity, String token){
		return responseBuilder.new Response<T>(payload, dalEntity, token);
	}
	
	/**
	 * Builder's entry point for login service
	 * @param payload
	 * @param dalEntity
	 * @param label
	 * @param data
	 * @return
	 */
	public static <T>Response<T> forData(T payload, DalEnum dalEntity, String label,T data){
		return responseBuilder.new Response<T>(payload, dalEntity, label,data);
	}	
	
	/**
	 * Builder's entry point for info services
	 * @param key of the attribute
	 * @param value of the attribute
	 * @return intermediate {@link InfoResponse} for further configuration
	 */
	public static InfoResponse forInfo(String key, String value){
		return responseBuilder.new InfoResponse(key, value);
	}

	/**
	 * Builder's entry point for info services
	 * @param payload to add as info
	 * @return intermediate {@link InfoResponse} for further configuration
	 */
	public static InfoResponse forInfo(Object payload){
		return responseBuilder.new InfoResponse(payload);
	}
	
	/**
	 * Builder's entry and end point for creating responses describing an error
	 * @param errorMessage to display in metadata
	 * @return BrAPI compilant error response
	 */
	public static DALResponse<String> forError(String errorMessage){
		//TODO verify error format for DAL
		return responseBuilder.new Response<String>(errorMessage,DalEnum.Error)
				.withNoPagination()
				.build();
	}
	
	/**
	 * Inner Class with calls for custom addition of elements in a response. Enforces the fluent
	 * use of the builder
	 * @author jarojas
	 *
	 * @param <T> the payload's class
	 */
	public class Response<T>{
		private DALResponse<T> response;
		
		private Response(T payload, DalEnum dalEntity){
			this.response = new DALResponse<>(dalEntity);
			this.response.setPayload(payload);
			response.setRecordMeta( Arrays.asList(new RecordMeta(dalEntity)));
		}
		
		
		private Response(T payload, DalEnum dalEntity, String token){
			this.response = new DALResponse<>(dalEntity);
			this.response.setPayload(payload);
			this.response.setWriteToken( Arrays.asList(new WriteToken(token)));
		}
		
		private Response(T payload, DalEnum dalEntity, String label, T data){
			this.response = new DALResponse<>(dalEntity);
			this.response.setPayload(payload);
			this.response.setRecordMeta( Arrays.asList(new RecordMeta(dalEntity)));
			this.response.setOtherMeta( label,data);
		}		
		
		public Response<T> withNoPagination(){
			return this;
		}
				
		/**
		 * Force to send pageNumber + 1, because DAL's request
		 * @param page
		 * @return
		 */
		public Response<T> withPagination(Page<?> page){
			response.setPagination( Arrays.asList(
					new Pagination(page.getSize(),
					page.getNumber()+1,
					page.getTotalElements(),
					page.getTotalPages())));
			return this;
		}
		
		/**
		 * Exit point of the builder.
		 * @return the {@link DALResponse} built
		 */
		public DALResponse<T> build(){
			
			return response;
		}
		
	}

	/**
	 * Inner Class for building a DALInfoResponse. Enforces the fluent
	 * use of the builder
	 * @author jarojas
	 *
	 * @param <T> the payload's class
	 */
	public class InfoResponse{
		private DALInfoResponse infoResponse;

		public InfoResponse(String key, String value) {
			infoResponse = new DALInfoResponse();
			infoResponse.put(key, value);
		}
		
		public InfoResponse(final Object payload) {
			infoResponse = new DALInfoResponse();
			for (Method method : payload.getClass().getDeclaredMethods()) {
				try {
					if(method.getName().startsWith("get")) {
						Optional.ofNullable(method.invoke(payload))
							.ifPresent(value -> infoResponse.put(
								method.getName().replace("get", ""),
								value.toString()));
					}
				} catch (Exception ex) { }
			
			
			}
		}

		
		/**
		 * Adds one more attribute into info element
		 * @param key
		 * @param value
		 * @return
		 */
		public InfoResponse and(String key, String value) {
			infoResponse.put(key, value);
			return this;
		}
		
		/**
		 * Exit point of the builder.
		 * @return the {@link DALInfoResponse} built
		 */
		public DALInfoResponse build() {
			return infoResponse;
		}
		
	}

}
