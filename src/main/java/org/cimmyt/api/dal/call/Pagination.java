/*
 * BMSAPI - BrAPI expansion
 * CIMMYT, 2016
 * License pending
 */
package org.cimmyt.api.dal.call;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * Models pagination section of a DAL response
 * @author jarojas
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
class Pagination {

	private int numPerPage;
	private int page;
	private long numOfRecords;
	private int numOfPages;
	
	public Pagination(){}

	public Pagination(int numPerPage, int page, long numOfRecords, int numOfPages) {
		super();
		this.numPerPage = numPerPage;
		this.page = page;
		this.numOfRecords = numOfRecords;
		this.numOfPages = numOfPages;
	}

	public int getNumPerPage() {
		return numPerPage;
	}

	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public long getNumOfRecords() {
		return numOfRecords;
	}

	public void setNumOfRecords(long numOfRecords) {
		this.numOfRecords = numOfRecords;
	}

	public int getNumOfPages() {
		return numOfPages;
	}

	public void setNumOfPages(int numOfPages) {
		this.numOfPages = numOfPages;
	}

}
