package org.cimmyt.api.dal.call;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * WriteToken object for DAL Login service response
 * @author RHTOLEDO
 *
 */

@JsonNaming(UpperCamelCaseStrategy.class)
class WriteToken {

	private String value;
	
	public WriteToken(String value){
		super();
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
    	
	
}
