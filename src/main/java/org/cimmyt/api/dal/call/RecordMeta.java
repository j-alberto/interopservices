/*
 * BMSAPI - BrAPI expansion
 * CIMMYT, 2016
 * License pending
 */
package org.cimmyt.api.dal.call;

import org.cimmyt.api.dal.to.DalEnum;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * Models record metadata section of a DAL response
 * @author jarojas
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
class RecordMeta {

	private DalEnum tagName;

	public RecordMeta(DalEnum tagName) {
		super();
		this.tagName = tagName;
	}

	public DalEnum getTagName() {
		return tagName;
	}

	public void setTagName(DalEnum tagName) {
		this.tagName = tagName;
	}

}
