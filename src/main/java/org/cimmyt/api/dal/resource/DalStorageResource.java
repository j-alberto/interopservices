package org.cimmyt.api.dal.resource;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.model.Container;
import org.cimmyt.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.cimmyt.api.dal.to.DalEnum.Storage;

/**
 * REST Resource in DAL format for {@link Container} entity
 * @author RHTOLEDO
 *
 */

@RestController
public class DalStorageResource {
	
	private ApiConversionService<Container> containverConverter;
	private ItemService containerService;
	
	@Autowired
	public DalStorageResource(ApiConversionService<Container> containverConverter,
		  	                  ItemService containerService){
		
		this.containverConverter = containverConverter;
		this.containerService    = containerService;
	}
	
	@RequestMapping(value="/dal/list/storage", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<Container>>> getContainers(){
		
		List<ApiTO<Container>> containers = containverConverter.convert(
				containerService.getContainers());
		
		return DalResponseBuilder.forData(containers, Storage)
				.withNoPagination()
				.build();
		
	}
	
}
