package org.cimmyt.api.dal.resource;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.model.Scale;
import org.cimmyt.model.ScaleValue;
import org.cimmyt.service.ScaleValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.cimmyt.api.dal.to.DalEnum.GeneralType;

/**
 * REST Resource in DAL format for {@link Scale} entity
 * @author RHTOLEDO
 *
 */

@RestController
public class DalTrialTypeResource {
	
	private ApiConversionService<ScaleValue> scaleConverter;
	private ScaleValueService studyTypeService;
	
	@Autowired
	public DalTrialTypeResource(ApiConversionService<ScaleValue> scaleConverter,
			                    ScaleValueService studyTypeService){
		
		this.scaleConverter   = scaleConverter;
		this.studyTypeService = studyTypeService;
	}
	
	@RequestMapping(value="/dal/list/type/trial/active", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<ScaleValue>>> findStudyTypes(){
		
		List<ApiTO<ScaleValue>> studyTypes = scaleConverter.convert(
				studyTypeService.getStudyType()
				);
		
		return DalResponseBuilder.forData(studyTypes, GeneralType).withNoPagination().build();
		
	}
	

}
