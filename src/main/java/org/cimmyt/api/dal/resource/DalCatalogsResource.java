package org.cimmyt.api.dal.resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.model.Program;
import org.cimmyt.service.SystemCatalogService;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;

import static org.cimmyt.api.dal.to.DalEnum.SystemGroup;

/**
 * REST Resource in DAL format for {@link Program} entity
 * @author RHTOLEDO
 *
 */

@RestController
public class DalCatalogsResource {
	
	private ApiConversionService<Program> programConverter;
	private SystemCatalogService catalogService;
	
	@Autowired
	public DalCatalogsResource(ApiConversionService<Program> programConverter,
			SystemCatalogService catalogService){
		
		this.programConverter = programConverter;
		this.catalogService   = catalogService;
	}
	
	@RequestMapping(value="/dal/list/group", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<Program>>> findPrograms(){
		
		List<ApiTO<Program>> programs = programConverter.convert(catalogService.getPrograms());
		
		return DalResponseBuilder.forData(programs, SystemGroup).withNoPagination().build();
		
	}
	
	
	

}
