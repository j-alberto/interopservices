package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.Operation;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.model.RestOperations;
import org.cimmyt.service.SystemCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link RestOperations} entity 
 * @author RHTOLEDO
 *
 */

@RestController
public class DalOperationResource {
	
	private ApiConversionService<RestOperations> restOperations;
	private SystemCatalogService operationService;
	
	@Autowired
	public DalOperationResource(ApiConversionService<RestOperations> restOperations,
			SystemCatalogService operationService){
		
		this.restOperations = restOperations;
		this.operationService = operationService;
	}	
	
	@RequestMapping(value="/dal/operation", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<RestOperations>>> getSwitchMsg(){
		
		List<ApiTO<RestOperations>> operationList = restOperations.convert(
				operationService.getOperations());
		
		return DalResponseBuilder.forData(operationList,Operation).withNoPagination().build();
	}	

}
