package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.TrialUnit;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.config.DalPageResquest;
import org.cimmyt.model.Plot;
import org.cimmyt.service.PlotService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link Study,Product} entity
 * @author RHTOLEDO
 *
 * TODO: Filter the canceled data
 */

@RestController
public class DalTrialUnitResource {

	private ApiConversionService<Plot> plotConverter;
	private PlotService plotService;
	
	public DalTrialUnitResource(ApiConversionService<Plot> plotConverter,
	           		            PlotService plotService) {
		this.plotConverter = plotConverter;
		this.plotService   = plotService;
	}
	
	@RequestMapping(value="/dal/trial/{trialId}/list/trialunit/{size}/page/{page}", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<Plot>>> findPlots(
			@PathVariable(required=true)int trialId,
			@PathVariable(required=true)int size,
			@PathVariable(required=true)int page){
		
		    Page<ApiTO<Plot>> plots = plotConverter.convert(
		    		plotService.getPlotsByStudyId(new DalPageResquest(page,size),trialId));
		    		
		
		    return DalResponseBuilder.forData(plots.getContent(), TrialUnit)
		    		.withPagination(plots)
		    		.build();
		
	}
	
}
