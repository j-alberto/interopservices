package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.Unit;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.config.DalPageResquest;
import org.cimmyt.model.Units;
import org.cimmyt.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link Units} entities
 * @author RHTOLEDO
 *
 */

@RestController
public class DalGeneralUnitResource {
	
	private ApiConversionService<Units> unitsConverter;
	private UnitService unitService;
	
	@Autowired
	public DalGeneralUnitResource(ApiConversionService<Units> unitsConverter,
			                      UnitService unitService){

		this.unitsConverter = unitsConverter;
 		this.unitService    = unitService;
 		
	}
	
	@RequestMapping(value="/dal/list/generalunit/{size}/page/{page}", method=RequestMethod.GET /*, produces = "text/xml"*/)
	public DALResponse<List<ApiTO<Units>>> findGeneralUnits(@PathVariable(required=true)int size,
			@PathVariable(required=true)int page) {
		
		Page<ApiTO<Units>> gralUnits = unitsConverter.convert(
				        unitService.getGeneralUnits(new DalPageResquest(page,size)));
		
		return DalResponseBuilder.forData(gralUnits.getContent(), Unit)
				.withPagination(gralUnits)
				.build();
		
	}
	
	

}
