package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.Site;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.config.DalPageResquest;
import org.cimmyt.model.Place;
import org.cimmyt.service.SystemCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link Place} entities
 * @author RHTOLEDO
 *
 */

@RestController
public class DalSiteResource {
	
	private ApiConversionService<Place> placeConverter;
	private SystemCatalogService locationService;
	
	@Autowired
	public DalSiteResource(ApiConversionService<Place> placeConverter,
			SystemCatalogService locationService){
		
		this.placeConverter  = placeConverter;
		this.locationService = locationService;
	}
	
	@RequestMapping(value="/dal/list/site/{size}/page/{page}", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<Place>>> findLocations(@PathVariable(required=true) int size,
			@PathVariable(required=true) int page){
		
		Page<ApiTO<Place>> locations = placeConverter.convert(
				locationService.getLocations(new DalPageResquest(page, size)));
		
		return DalResponseBuilder.forData(locations.getContent(), Site)
				.withPagination(locations)
				.build();
		
	}
	

}
