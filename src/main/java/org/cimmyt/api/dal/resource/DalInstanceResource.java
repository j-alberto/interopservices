package org.cimmyt.api.dal.resource;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.model.PlotData;
import org.cimmyt.service.PlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.cimmyt.api.dal.to.DalEnum.InstanceNumber;

/**
 * REST Resource in DAL format for {@link PlotData} entities 
 * @author RHTOLEDO
 *
 */
@RestController
public class DalInstanceResource {
	
	private ApiConversionService<PlotData> plotDataConverter;
	private PlotService plotDataService;
	
	
	@Autowired
	public DalInstanceResource(ApiConversionService<PlotData> plotDataConverter,
			PlotService plotDataService){
		
		this.plotDataConverter = plotDataConverter;
		this.plotDataService   = plotDataService;
	}
	
	
	@RequestMapping(value="/dal/trial/{trialId}/list/instancenumber", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<PlotData>>> getInstanceNumber(
			                                 @PathVariable(required=true)int trialId){
		
		List<ApiTO<PlotData>> instances = plotDataConverter.convert(plotDataService.getPlotDataById(trialId));
		
		return DalResponseBuilder.forData(instances, InstanceNumber)
				.withNoPagination()
				.build();
		
	}
}
