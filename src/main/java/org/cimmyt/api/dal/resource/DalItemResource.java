package org.cimmyt.api.dal.resource;

import java.util.List;

import static org.cimmyt.api.dal.to.DalEnum.Item;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.config.DalPageResquest;
import org.cimmyt.model.SeedStorage;
import org.cimmyt.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link SeedStorage} entity
 * @author RHTOLEDO
 *
 */

@RestController
public class DalItemResource {
	
	private ApiConversionService<SeedStorage> seedStorageConverter;
	private ItemService seedStorageService;
	
	@Autowired
	public DalItemResource(ApiConversionService<SeedStorage> seedStorageConverter,
			ItemService seedStorageService){
		
		this.seedStorageConverter = seedStorageConverter;
		this.seedStorageService   = seedStorageService;
		
	}
	
	
	@RequestMapping(value="/dal/list/item/{size}/page/{page}", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<SeedStorage>>> findItems(@PathVariable(required=true) int size,
			                                               @PathVariable(required=true) int page){
		
		Page<ApiTO<SeedStorage>> items = seedStorageConverter.convert(
				seedStorageService.getSeedStorage(new DalPageResquest(page,size)));
		
		return DalResponseBuilder.forData(items.getContent(), Item)
				.withPagination(items)
				.build();
		
	}
	
	

}
