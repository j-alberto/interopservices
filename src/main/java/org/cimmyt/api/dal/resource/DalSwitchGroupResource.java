package org.cimmyt.api.dal.resource;

import org.cimmyt.api.dal.call.DALInfoResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST resource in DAL format to set a groupId selected,in this moment 
 * a dummy response is enough 
 * @author RHTOLEDO
 *
 */

@RestController
public class DalSwitchGroupResource {
	
	@Autowired
	public DalSwitchGroupResource(){}	
	
	@RequestMapping(value="/dal/switch/group/{groupId}", method=RequestMethod.GET,produces = "text/xml")
	public DALInfoResponse getSwitchMsg(
			                 @PathVariable(required=true) String groupId){
		
		return DalResponseBuilder.forInfo("Message", "You have been switched successfully.").build();
		
	}
}
