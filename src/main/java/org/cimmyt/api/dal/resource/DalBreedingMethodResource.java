package org.cimmyt.api.dal.resource;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.model.BreedingMethodVariable;
import org.cimmyt.service.SystemCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.cimmyt.api.dal.to.DalEnum.BreedingMethod;
/**
 * REST Resource in DAL format for {@link BreedingMethodVariable} entity
 * @author RHTOLEDO
 *
 */

@RestController
public class DalBreedingMethodResource {
	
	
	private ApiConversionService<BreedingMethodVariable> breedingMethodConverter;
	private SystemCatalogService systemCatalogService;
	
	@Autowired
	public DalBreedingMethodResource(ApiConversionService<BreedingMethodVariable> breedingMethodConverter,
			SystemCatalogService systemCatalogService) {
		super();
		this.breedingMethodConverter = breedingMethodConverter;
		this.systemCatalogService = systemCatalogService;
	}
	
	@RequestMapping(value="/dal/list/breedingmethod", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<BreedingMethodVariable>>> getData(){
		
		List<ApiTO<BreedingMethodVariable>> methods = breedingMethodConverter.convert(
				                       systemCatalogService.getBreedingMethod());
		
		return DalResponseBuilder.forData(methods, BreedingMethod)
				.withNoPagination()
				.build();
		
	}
	

}
