package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.OutputFile;

import java.util.Arrays;
import java.util.List;

import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.api.dal.to.OutputFileCSV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link OutputFile} entity
 * @author RHTOLEDO
 *
 */

@RestController
public class DalExportCSVFileResource {
	
	private static String fileName = "testAuto";
	
	@Autowired
	public DalExportCSVFileResource(){}
	
	@RequestMapping(value="/dal/export/samplemeasurement/csv", method={RequestMethod.POST,RequestMethod.GET} )
	public DALResponse<List<OutputFileCSV>> getURLForSamples(){
		
		OutputFileCSV ofile = OutputFileCSV.getInstance();
		ofile.setCsv("http://itu-dev-srv:1000/" + fileName + ".csv");
		return DalResponseBuilder.forData(Arrays.asList(ofile),OutputFile).withNoPagination().build();
		
	}

}
