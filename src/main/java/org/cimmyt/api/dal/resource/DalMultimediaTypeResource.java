package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.GeneralType;

import java.util.ArrayList;
import java.util.List;

import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.api.dal.to.GeneralType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author RHTOLEDO
 *
 */
@RestController
public class DalMultimediaTypeResource {
	
	
	@Autowired
	public DalMultimediaTypeResource(){}
	
	
	@RequestMapping(value="/dal/list/type/multimedia/active", method=RequestMethod.GET)
	public DALResponse<List<GeneralType>> getSwitchMsg(){
		
		
		List<GeneralType> gtList = new ArrayList<>();
		
		GeneralType gt = new GeneralType();
		
		gt.setClase("multimedia");
		gt.setTypeId(1);
		gt.setIsTypeActive(1);
		gt.setTypeName("text");
		gt.setTypeNote("test type note");
		gt.setUpdate("update/type/multimedia/1");
		
		gtList.add(gt);
		
		return DalResponseBuilder.forData(gtList, GeneralType).withNoPagination().build();
		 
		
	}	

}
