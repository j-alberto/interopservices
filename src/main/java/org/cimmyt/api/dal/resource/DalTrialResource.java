package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.Trial;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.config.DalPageResquest;
import org.cimmyt.model.Study;
import org.cimmyt.service.StudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * REST Resource in DAL format for {@link Study} entities
 * @author jarojas
 *
 */
@RestController
public class DalTrialResource {
	
	private ApiConversionService<Study> studyConverter;
	private StudyService studyService;
	
	@Autowired
	public DalTrialResource(ApiConversionService<Study> studyConverter,
			StudyService studyService) {
		this.studyConverter = studyConverter;
		this.studyService   = studyService;
	}
	
	@RequestMapping(value="/dal/list/trial/{size}/page/{page}", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<Study>>> findStudies( @PathVariable(required=true)int size,
			@PathVariable(required=true)int page) {
		
		Page<ApiTO<Study>> studies = studyConverter.convert(
				studyService.getStudies(new DalPageResquest(page, size)));
		
		return DalResponseBuilder.forData(studies.getContent(), Trial)
				.withPagination(studies)
				.build();
				
	}
	
}
