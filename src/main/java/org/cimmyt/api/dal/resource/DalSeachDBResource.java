package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.specimen_genotype;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.api.dal.to.StatInfo;
import org.cimmyt.model.SearchInfo;
import org.cimmyt.service.SystemCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO: Check the correct functionality to this resource
 * 
 * @author RHTOLEDO
 *
 */

@RestController
public class DalSeachDBResource {
	
	
	private ApiConversionService<SearchInfo> searchConverter;
	private SystemCatalogService searchService;
	
	@Autowired
	public DalSeachDBResource(ApiConversionService<SearchInfo> searchConverter,
			SystemCatalogService searchService){
		
		this.searchConverter = searchConverter;
		this.searchService   = searchService;
		
	}
	
	@RequestMapping(value="/dal/search/db/{size}/page/{page}", method=RequestMethod.GET)
	public DALResponse<Object> getSwitchMsg(
			@PathVariable(required=true)int size,
			@PathVariable(required=true)int page){
		
		
		List<ApiTO<SearchInfo>> searchInfo = searchConverter.convert(
				searchService.getSearchInfo());
		
		return  DalResponseBuilder.forData(searchInfo,
				                           specimen_genotype,
				                           "StatInfo",
				                           new StatInfo("second","0.63"))
				.withNoPagination()
				.build();
	}	

}
