package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.TrialTrait;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.model.Variable;
import org.cimmyt.service.TraitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link Variable} entities,
 * filtered by type=observations, see {@link TraitServiceImpl#getTraitList(Integer)}
 * @author RHTOLEDO
 * 
 */

@RestController
public class DalTraitResource {
	
	
	private ApiConversionService<Variable> traitConverter;
	private TraitService traitService;
	
	@Autowired
	public DalTraitResource(ApiConversionService<Variable> traitConverter,
			                TraitService traitService){
		
		this.traitConverter = traitConverter;
		this.traitService   = traitService;
		
	}
	
	@RequestMapping(value="/dal/trial/{trialId}/list/trait", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<Variable>>> findTrait(
			@PathVariable(required=true) int trialId){
		
		List<ApiTO<Variable>> traits = traitConverter.convert(
				traitService.getTraitList(trialId));
		
		/*return DalResponseBuilder.forData(traits, Trait)
				.withNoPagination()
				.build();*/
		
		return DalResponseBuilder.forData(traits, TrialTrait)
				.withNoPagination()
				.build();
		
	}

}
