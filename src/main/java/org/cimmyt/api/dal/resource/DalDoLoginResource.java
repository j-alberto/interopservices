package org.cimmyt.api.dal.resource;

import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.model.User;
import org.cimmyt.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The method {@link #doLogin(String)}, implement a fixed token string,
 * this need to be replaced when the security features are implemented. 
 * 
 * @author RHTOLEDO
 * @version 1.0 
 *
 */
@RestController
public class DalDoLoginResource {
	
	//private final String token = "1a0fc061fceb45bfb3ba7bab711b7a1d";
	
	//private ApiConversionService<User> userConverter;
	//private LoginService loginService;
	
	
	@Autowired
	public DalDoLoginResource(ApiConversionService<User> userConverter, 
			LoginService loginService) {

		//this.userConverter = userConverter;
		//this.loginService  = loginService;
	}

	@RequestMapping(value="/dal/login/{user}/no", method=RequestMethod.POST, produces = "text/xml")
	//public DALResponse<List<ApiTO<User>>> doLogin(
	public ResponseEntity<String> doLogin(
			@PathVariable(required=true) String user){
		
        //List<ApiTO<User>> userLogin = userConverter.convert(loginService.getUserIdByName(user)); 		
		
        return new ResponseEntity<String>("<DATA><User UserId=\"1\"/><WriteToken Value=\"b200ec539df246f2b1f36684ec94d557\"/></DATA>", HttpStatus.OK); 
        
		//return  DalResponseBuilder.forData(userLogin, User, token).withNoPagination().build();
	}

}
