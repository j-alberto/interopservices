package org.cimmyt.api.dal.resource;

import static org.cimmyt.api.dal.to.DalEnum.TrialUnit;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.dal.call.DALResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.cimmyt.model.Plot;
import org.cimmyt.service.PlotService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL format for {@link Study,Product} entities
 * @author RHTOLEDO
 *
 */

@RestController
public class DalTrialUnitResourceNoPag {
	
	private ApiConversionService<Plot> plotConverter;
	private PlotService plotService;
	
	public DalTrialUnitResourceNoPag(ApiConversionService<Plot> plotConverter,
	            PlotService plotService){
		
		this.plotConverter = plotConverter;
		this.plotService   = plotService;
		
	}
	
	@RequestMapping(value="/dal/trial/{trialId}/list/trialunit", method=RequestMethod.GET)
	public DALResponse<List<ApiTO<Plot>>> findPlots(@PathVariable(required=true)int trialId){
		
		    List<ApiTO<Plot>> plots = plotConverter.convert(
		    		plotService.getPlotsByStudyId(trialId));
		    		
		    return DalResponseBuilder.forData(plots, TrialUnit)
		    		.withNoPagination()
		    		.build();
		
	}	

}
