package org.cimmyt.api.dal.resource;

import org.cimmyt.api.dal.call.DALInfoResponse;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in DAL XML format about DAL version info
 * @author RHTOLEDO
 *
 */
@RestController
public class DalVersionResource {
	
	@Autowired
	public DalVersionResource(){
	}	
	
	@RequestMapping(value="/dal/get/version", method=RequestMethod.GET,produces = "text/xml")
	public DALInfoResponse getSwitchMsg(){
		
		return DalResponseBuilder.forInfo("Copyright","Copyright (c) 2011-2014, Diversity Arrays Technology, All rights reserved.")
		                      .and("Version","0.1")
		                      .and("ServerVersion", "1.0.1")
		                      .and("About", "Data Access Layer")
		                      .build();
		
	}
}
