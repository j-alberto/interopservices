package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Container;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 *
 */

@JsonNaming(UpperCamelCaseStrategy.class)
public class Storage implements ApiTO<Container> {
	
	
	private String  storageDetails;
    private String  storageNote;
    private String  update;
    private String  storageBarcode;
    private String  delete;
    private String  storageLocation;
    private Integer storageId;
    private Integer storageParentId;
    
	public String getStorageDetails() {
		return storageDetails;
	}
	public void setStorageDetails(String storageDetails) {
		this.storageDetails = storageDetails;
	}
	public String getStorageNote() {
		return storageNote;
	}
	public void setStorageNote(String storageNote) {
		this.storageNote = storageNote;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public String getStorageBarcode() {
		return storageBarcode;
	}
	public void setStorageBarcode(String storageBarcode) {
		this.storageBarcode = storageBarcode;
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	public String getStorageLocation() {
		return storageLocation;
	}
	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}
	public Integer getStorageId() {
		return storageId;
	}
	public void setStorageId(Integer storageId) {
		this.storageId = storageId;
	}
	public Integer getStorageParentId() {
		return storageParentId;
	}
	public void setStorageParentId(Integer storageParentId) {
		this.storageParentId = storageParentId;
	}
    
    
	
	

}
