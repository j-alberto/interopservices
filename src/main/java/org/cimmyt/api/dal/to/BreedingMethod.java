package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.BreedingMethodVariable;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * {@link ApiTO Transfer Object} for {@link BreedingMethodVariable} in DAL calls
 * @author RHTOLEDO
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class BreedingMethod implements ApiTO<BreedingMethodVariable>{

	private String  update;
    private Integer breedingMethodId;
    private String  breedingMethodNote;
    private String  breedingMethodName;
      
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public Integer getBreedingMethodId() {
		return breedingMethodId;
	}
	public void setBreedingMethodId(Integer breedingMethodId) {
		this.breedingMethodId = breedingMethodId;
	}
	public String getBreedingMethodNote() {
		return breedingMethodNote;
	}
	public void setBreedingMethodNote(String breedingMethodNote) {
		this.breedingMethodNote = breedingMethodNote;
	}
	public String getBreedingMethodName() {
		return breedingMethodName;
	}
	public void setBreedingMethodName(String breedingMethodName) {
		this.breedingMethodName = breedingMethodName;
	}
      
}
