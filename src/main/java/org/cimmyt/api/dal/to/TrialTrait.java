package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Variable;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * New request in DAL documentation
 * @author RHTOLEDO
 *
 */

@JsonNaming(UpperCamelCaseStrategy.class)
public class TrialTrait implements ApiTO<Variable>{
	
	private String  traitName;
    private String  traitValRule;
    private Integer compulsory;
    private Integer trialTraitId;
    private String  delete;
    private Integer traitId;
    private String  update;
    private Integer traitUnit;
    private Integer traitValueMaxLength;
    private Integer trialId;
    private String  traitDataType;
	public String getTraitName() {
		return traitName;
	}
	public void setTraitName(String traitName) {
		this.traitName = traitName;
	}
	public String getTraitValRule() {
		return traitValRule;
	}
	public void setTraitValRule(String traitValRule) {
		this.traitValRule = traitValRule;
	}
	public Integer getCompulsory() {
		return compulsory;
	}
	public void setCompulsory(Integer compulsory) {
		this.compulsory = compulsory;
	}
	public Integer getTrialTraitId() {
		return trialTraitId;
	}
	public void setTrialTraitId(Integer trialTraitId) {
		this.trialTraitId = trialTraitId;
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	public Integer getTraitId() {
		return traitId;
	}
	public void setTraitId(Integer traitId) {
		this.traitId = traitId;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public Integer getTraitUnit() {
		return traitUnit;
	}
	public void setTraitUnit(Integer traitUnit) {
		this.traitUnit = traitUnit;
	}
	public Integer getTraitValueMaxLength() {
		return traitValueMaxLength;
	}
	public void setTraitValueMaxLength(Integer traitValueMaxLength) {
		this.traitValueMaxLength = traitValueMaxLength;
	}
	public Integer getTrialId() {
		return trialId;
	}
	public void setTrialId(Integer trialId) {
		this.trialId = trialId;
	}
	public String getTraitDataType() {
		return traitDataType;
	}
	public void setTraitDataType(String traitDataType) {
		this.traitDataType = traitDataType;
	}
    
    
}
