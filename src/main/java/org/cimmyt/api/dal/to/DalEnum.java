package org.cimmyt.api.dal.to;

import org.cimmyt.api.dal.call.DalResponseBuilder;

/**
 * Available DAL entities which can be used in responses in {@link DalResponseBuilder}
 * @author jarojas
 *
 */
public enum DalEnum {
	Trial, Site , SystemGroup, Error, TrialUnit, Specimen, GeneralType, Trait, User, Info, Operation, Unit, specimen_genotype,
	Item, Storage, InstanceNumber,TrialTrait,OutputFile, BreedingMethod
}
