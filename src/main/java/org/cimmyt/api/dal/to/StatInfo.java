package org.cimmyt.api.dal.to;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(UpperCamelCaseStrategy.class)
public class StatInfo {
	
	private String unit;
	private String serverElapsedTime;
	
	public StatInfo(String unit,String serverElapsedTime){
		this.unit = unit;
		this.serverElapsedTime = serverElapsedTime;
	}
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getServerElapsedTime() {
		return serverElapsedTime;
	}
	public void setServerElapsedTime(String serverElapsedTime) {
		this.serverElapsedTime = serverElapsedTime;
	}
	
	

}
