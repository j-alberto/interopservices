package org.cimmyt.api.dal.to;

public class OutputFileCSV {
	
	private String csv;
	
	private OutputFileCSV(){}
	
	public static OutputFileCSV getInstance(){
		return new OutputFileCSV();
	}

	public String getCsv() {
		return csv;
	}

	public void setCsv(String csv) {
		this.csv = csv;
	}
	
}
