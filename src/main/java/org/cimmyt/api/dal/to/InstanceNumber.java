package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.PlotData;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * {@link ApiTO Transfer Object} for {@link PlotData} in DAL calls
 * @author RHTOLEDO
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class InstanceNumber implements ApiTO<PlotData>{
	
	private Integer traitId;
	private Integer instanceNumber;
	private Integer sampleTypeId;
	
	public Integer getTraitId() {
		return traitId;
	}
	public void setTraitId(Integer traitId) {
		this.traitId = traitId;
	}
	public Integer getInstanceNumber() {
		return instanceNumber;
	}
	public void setInstanceNumber(Integer instanceNumber) {
		this.instanceNumber = instanceNumber;
	}
	public Integer getSampleTypeId() {
		return sampleTypeId;
	}
	public void setSampleTypeId(Integer sampleTypeId) {
		this.sampleTypeId = sampleTypeId;
	}
	
	
	
	

}
