package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.User;

/**
 * 
 * @author RHTOLEDO
 *
 */
public class UserDal implements ApiTO<User> {
	
	private Integer userId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
