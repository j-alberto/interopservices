package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Program;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * {@link ApiTo Transfer Object} for {@link Program} in DAL calls
 * @author RHTOLEDO
 * 
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class SystemGroup implements ApiTO<Program>{
	
	private String systemGroupName;
	private String systemGroupDescription;
	private Integer systemGroupId;
	
	public String getSystemGroupName() {
		return systemGroupName;
	}
	public void setSystemGroupName(String systemGroupName) {
		this.systemGroupName = systemGroupName;
	}
	public String getSystemGroupDescription() {
		return systemGroupDescription;
	}
	public void setSystemGroupDescription(String systemGroupDescription) {
		this.systemGroupDescription = systemGroupDescription;
	}
	public Integer getSystemGroupId() {
		return systemGroupId;
	}
	public void setSystemGroupId(Integer systemGroupId) {
		this.systemGroupId = systemGroupId;
	}
	
	
	
	

}
