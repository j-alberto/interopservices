package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.SearchInfo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(UpperCamelCaseStrategy.class)
public class Specimen_Genotype implements ApiTO<SearchInfo>{
	
	private String  solrId;
    private Integer breedingMethodId;
    private Integer specimenId;
    private String  entity_name;
    private String  specimenName;
    private String  genotypeName;
    private Integer filialGeneration;
    private Integer genotypeId;
    private Integer isActive;
    private Integer genusId;
    private String  _version_;
    private String  specimenBarcode;
    
	public String getSolrId() {
		return solrId;
	}
	public void setSolrId(String solrId) {
		this.solrId = solrId;
	}
	public Integer getBreedingMethodId() {
		return breedingMethodId;
	}
	public void setBreedingMethodId(Integer breedingMethodId) {
		this.breedingMethodId = breedingMethodId;
	}
	public Integer getSpecimenId() {
		return specimenId;
	}
	public void setSpecimenId(Integer specimenId) {
		this.specimenId = specimenId;
	}

	public String getEntity_name() {
		return entity_name;
	}
	public void setEntity_name(String entity_name) {
		this.entity_name = entity_name;
	}
	public String getSpecimenName() {
		return specimenName;
	}
	public void setSpecimenName(String specimenName) {
		this.specimenName = specimenName;
	}
	public String getGenotypeName() {
		return genotypeName;
	}
	public void setGenotypeName(String genotypeName) {
		this.genotypeName = genotypeName;
	}
	public Integer getFilialGeneration() {
		return filialGeneration;
	}
	public void setFilialGeneration(Integer filialGeneration) {
		this.filialGeneration = filialGeneration;
	}
	public Integer getGenotypeId() {
		return genotypeId;
	}
	public void setGenotypeId(Integer genotypeId) {
		this.genotypeId = genotypeId;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getGenusId() {
		return genusId;
	}
	public void setGenusId(Integer genusId) {
		this.genusId = genusId;
	}

	public String get_version_() {
		return _version_;
	}
	public void set_version_(String _version_) {
		this._version_ = _version_;
	}
	public String getSpecimenBarcode() {
		return specimenBarcode;
	}
	public void setSpecimenBarcode(String specimenBarcode) {
		this.specimenBarcode = specimenBarcode;
	}
    
    
    

}
