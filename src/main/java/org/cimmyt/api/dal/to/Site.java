package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Place;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * {@link ApiTo Transfer Object} for {@link Place}
 * @author RHTOLEDO
 *
 */

@JsonNaming(UpperCamelCaseStrategy.class)
public class Site implements ApiTO<Place> {
	
    private String  siteTypeId;
	private Integer currentSiteManagerId;
    private Integer siteId;
    private String  siteStartDate;
    private String  siteTypeName;
    private String  update;
    private String  sitelocation;
    private Double  latitude;
    private String  siteAcronym;
    private String  siteName;
    private String  currentSiteManagerName;
    private String  siteEndDate;
    private Double  longitude;
    
    
	public String getSiteTypeId() {
		return siteTypeId;
	}
	public void setSiteTypeId(String siteTypeId) {
		this.siteTypeId = siteTypeId;
	}
	public Integer getCurrentSiteManagerId() {
		return currentSiteManagerId;
	}
	public void setCurrentSiteManagerId(Integer currentSiteManagerId) {
		this.currentSiteManagerId = currentSiteManagerId;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public String getSiteStartDate() {
		return siteStartDate;
	}
	public void setSiteStartDate(String siteStartDate) {
		this.siteStartDate = siteStartDate;
	}
	public String getSiteTypeName() {
		return siteTypeName;
	}
	public void setSiteTypeName(String siteTypeName) {
		this.siteTypeName = siteTypeName;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public String getSitelocation() {
		return sitelocation;
	}
	public void setSitelocation(String sitelocation) {
		this.sitelocation = sitelocation;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public String getSiteAcronym() {
		return siteAcronym;
	}
	public void setSiteAcronym(String siteAcronym) {
		this.siteAcronym = siteAcronym;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getCurrentSiteManagerName() {
		return currentSiteManagerName;
	}
	public void setCurrentSiteManagerName(String currentSiteManagerName) {
		this.currentSiteManagerName = currentSiteManagerName;
	}
	public String getSiteEndDate() {
		return siteEndDate;
	}
	public void setSiteEndDate(String siteEndDate) {
		this.siteEndDate = siteEndDate;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}	
	
	
	
}
