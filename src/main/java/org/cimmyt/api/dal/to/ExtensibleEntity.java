package org.cimmyt.api.dal.to;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

/**
 * <p>Superset of DAL entities to provide extra attributes like HEATEOAS links, 
 * audit fields, security flags, etc.</p>
 * Currently there are not restrictions on what can be added here. (Probably in next versions)
 * @author jarojas
 *
 */
abstract class ExtensibleEntity {

	private Map<String, String> extra;

	ExtensibleEntity() {
		super();
		extra = new HashMap<>();
	}

	@JsonAnyGetter
	public Map<String, String> getExtra() {
		return extra;
	}

	public void addExtra(String key, String value) {
		extra.put(key, value);
	}
	
	/**
	 * example of a fixed optional attribute. 
	 * @param link
	 */
	//TODO create mechanism to infer urls, so it only needs the Id
	public void setUpdateLink(String link) {
		extra.put("update", link);
	}
	
}
