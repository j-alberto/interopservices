package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Study;

/**
 * 
 * @author RHTOLEDO
 *
 */

public class TrialType implements ApiTO<Study>{
	
	private Integer isTypeActive;
	private Integer typeId;
	private String  typeName;
	private String  typeNote;
	
	public Integer getIsTypeActive() {
		return isTypeActive;
	}
	public void setIsTypeActive(Integer isTypeActive) {
		this.isTypeActive = isTypeActive;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeNote() {
		return typeNote;
	}
	public void setTypeNote(String typeNote) {
		this.typeNote = typeNote;
	}	
	
	

}
