package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Plot;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 * 
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class TrialUnit implements ApiTO<Plot> {
	
	private String  ultimatePerm;
	private String  addSpecimen;
	private String  ultimatePermission;
	private String  trialUnitBarcode;
	private String  update;
	private Integer sourceTrialUnitId;
	private Integer trialUnitId;
	private Double  longitude;
	private Integer replicateNumber;
	private String  listSpecimen;
	private String  trialUnitNote;
	private String  treatmentText;
	private Integer siteId;
	private Integer treatmentId;
	private Integer unitPositionId;
	private Integer sampleSupplierId;
	private Integer trialId;
	private Double  latitude;
	private String  unitPositionText;
	private Specimen specimen;
    private String  siteName;
    private Integer trialUnitX;
    private Integer trialUnitY;
    private String  trialUnitPosition;
    private String  addKeyword;
    private Integer trialUnitZ;
    private Integer trialUnitEntryId;
    private String  trialunitlocation;
    
	public String getUltimatePerm() {
		return ultimatePerm;
	}
	public void setUltimatePerm(String ultimatePerm) {
		this.ultimatePerm = ultimatePerm;
	}
	public String getAddSpecimen() {
		return addSpecimen;
	}
	public void setAddSpecimen(String addSpecimen) {
		this.addSpecimen = addSpecimen;
	}
	public String getUltimatePermission() {
		return ultimatePermission;
	}
	public void setUltimatePermission(String ultimatePermission) {
		this.ultimatePermission = ultimatePermission;
	}
	public String getTrialUnitBarcode() {
		return trialUnitBarcode;
	}
	public void setTrialUnitBarcode(String trialUnitBarcode) {
		this.trialUnitBarcode = trialUnitBarcode;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public Integer getSourceTrialUnitId() {
		return sourceTrialUnitId;
	}
	public void setSourceTrialUnitId(Integer sourceTrialUnitId) {
		this.sourceTrialUnitId = sourceTrialUnitId;
	}
	public Integer getTrialUnitId() {
		return trialUnitId;
	}
	public void setTrialUnitId(Integer trialUnitId) {
		this.trialUnitId = trialUnitId;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Integer getReplicateNumber() {
		return replicateNumber;
	}
	public void setReplicateNumber(Integer replicateNumber) {
		this.replicateNumber = replicateNumber;
	}
	public String getListSpecimen() {
		return listSpecimen;
	}
	public void setListSpecimen(String listSpecimen) {
		this.listSpecimen = listSpecimen;
	}
	public String getTrialUnitNote() {
		return trialUnitNote;
	}
	public void setTrialUnitNote(String trialUnitNote) {
		this.trialUnitNote = trialUnitNote;
	}
	public String getTreatmentText() {
		return treatmentText;
	}
	public void setTreatmentText(String treatmentText) {
		this.treatmentText = treatmentText;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public Integer getTreatmentId() {
		return treatmentId;
	}
	public void setTreatmentId(Integer treatmentId) {
		this.treatmentId = treatmentId;
	}
	public Integer getUnitPositionId() {
		return unitPositionId;
	}
	public void setUnitPositionId(Integer unitPositionId) {
		this.unitPositionId = unitPositionId;
	}
	public Integer getSampleSupplierId() {
		return sampleSupplierId;
	}
	public void setSampleSupplierId(Integer sampleSupplierId) {
		this.sampleSupplierId = sampleSupplierId;
	}
	public Integer getTrialId() {
		return trialId;
	}
	public void setTrialId(Integer trialId) {
		this.trialId = trialId;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public String getUnitPositionText() {
		return unitPositionText;
	}
	public void setUnitPositionText(String unitPositionText) {
		this.unitPositionText = unitPositionText;
	}
	public Specimen getSpecimen() {
		return specimen;
	}
	public void setSpecimen(Specimen specimen) {
		this.specimen = specimen;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public Integer getTrialUnitX() {
		return trialUnitX;
	}
	public void setTrialUnitX(Integer trialUnitX) {
		this.trialUnitX = trialUnitX;
	}
	public Integer getTrialUnitY() {
		return trialUnitY;
	}
	public void setTrialUnitY(Integer trialUnitY) {
		this.trialUnitY = trialUnitY;
	}
	public String getTrialUnitPosition() {
		return trialUnitPosition;
	}
	public void setTrialUnitPosition(String trialUnitPosition) {
		this.trialUnitPosition = trialUnitPosition;
	}
	public String getAddKeyword() {
		return addKeyword;
	}
	public void setAddKeyword(String addKeyword) {
		this.addKeyword = addKeyword;
	}
	public Integer getTrialUnitZ() {
		return trialUnitZ;
	}
	public void setTrialUnitZ(Integer trialUnitZ) {
		this.trialUnitZ = trialUnitZ;
	}
	public Integer getTrialUnitEntryId() {
		return trialUnitEntryId;
	}
	public void setTrialUnitEntryId(Integer trialUnitEntryId) {
		this.trialUnitEntryId = trialUnitEntryId;
	}
	public String getTrialunitlocation() {
		return trialunitlocation;
	}
	public void setTrialunitlocation(String trialunitlocation) {
		this.trialunitlocation = trialunitlocation;
	}	
	
    
    

}
