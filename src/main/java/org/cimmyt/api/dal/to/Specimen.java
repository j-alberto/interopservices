package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Study;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 * 
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class Specimen implements ApiTO<Study>{
	
	private String  specimenName;
	private String  plantDate;
	private Integer trialUnitId;
	private String  hasDied;
	private String  itemId;
	private Integer specimenId;
	private String  notes;
	private String  harvestDate;
	private Integer trialUnitSpecimenId;
	
	public String getSpecimenName() {
		return specimenName;
	}
	public void setSpecimenName(String specimenName) {
		this.specimenName = specimenName;
	}
	public String getPlantDate() {
		return plantDate;
	}
	public void setPlantDate(String plantDate) {
		this.plantDate = plantDate;
	}
	public Integer getTrialUnitId() {
		return trialUnitId;
	}
	public void setTrialUnitId(Integer trialUnitId) {
		this.trialUnitId = trialUnitId;
	}
	public String getHasDied() {
		return hasDied;
	}
	public void setHasDied(String hasDied) {
		this.hasDied = hasDied;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Integer getSpecimenId() {
		return specimenId;
	}
	public void setSpecimenId(Integer specimenId) {
		this.specimenId = specimenId;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getHarvestDate() {
		return harvestDate;
	}
	public void setHarvestDate(String harvestDate) {
		this.harvestDate = harvestDate;
	}
	public Integer getTrialUnitSpecimenId() {
		return trialUnitSpecimenId;
	}
	public void setTrialUnitSpecimenId(Integer trialUnitSpecimenId) {
		this.trialUnitSpecimenId = trialUnitSpecimenId;
	}	
	
	
	
	

}
