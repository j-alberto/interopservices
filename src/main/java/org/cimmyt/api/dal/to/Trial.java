package org.cimmyt.api.dal.to;

import java.sql.Timestamp;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Study;
import org.cimmyt.model.StudyMetadata;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * {@link ApiTO Transfer Object} for {@link StudyMetadata} in DAL calls
 * @author jarojas
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class Trial extends ExtensibleEntity implements ApiTO<Study> {

	
	private String    trialNote;
	private String    trialAcronym;
	private Timestamp trialEndDate;
	private String    trialTypeId;
	private Timestamp trialStartDate;
	private String    trialName;
	private Integer   trialId;
	private Integer   trialManagerId;
	private String    trialManagerName;
	private String    designTypeName;
	private String    siteName;
	private Integer   siteId;
	private String    trialTypeName;
	private Integer   designTypeId;
	private Integer   currentWorkflowId;
	private String    ownGroupPermission;
	private String    listTrialUnit;
	private Integer   accessGroupId;
	private String    accessGroupName;
	private String    accessGroupPermission;
	private String    accessGroupPerm;
	private String    map;
	private String    addTrait;
	private Integer   ownGroupId;
	private String    triallocation;
	private Integer   ownGroupPerm;
	private String    ownGroupName;
	private Double    longitude;
	private String    update;
	private Integer   trialNumber;
	private Integer   ultimatePerm;
	private String    chgPerm;
	private String    ultimatePermission;
	private String    otherPermission;
	private String    projectName;
	private Double    latitude;
	private String    chgOwner;
	private Integer   projectId;
	private Integer   otherPerm;
	
	public String getTrialNote() {
		return trialNote;
	}
	public void setTrialNote(String trialNote) {
		this.trialNote = trialNote;
	}
	public String getTrialAcronym() {
		return trialAcronym;
	}
	public void setTrialAcronym(String trialAcronym) {
		this.trialAcronym = trialAcronym;
	}
	public Timestamp getTrialEndDate() {
		return trialEndDate;
	}
	public void setTrialEndDate(Timestamp trialEndDate) {
		this.trialEndDate = trialEndDate;
	}
	public String getTrialTypeId() {
		return trialTypeId;
	}
	public void setTrialTypeId(String trialTypeId) {
		this.trialTypeId = trialTypeId;
	}
	public Timestamp getTrialStartDate() {
		return trialStartDate;
	}
	public void setTrialStartDate(Timestamp trialStartDate) {
		this.trialStartDate = trialStartDate;
	}
	public String getTrialName() {
		return trialName;
	}
	public void setTrialName(String trialName) {
		this.trialName = trialName;
	}
	public Integer getTrialId() {
		return trialId;
	}
	public void setTrialId(Integer trialId) {
		this.trialId = trialId;
	}
	public Integer getTrialManagerId() {
		return trialManagerId;
	}
	public void setTrialManagerId(Integer trialManagerId) {
		this.trialManagerId = trialManagerId;
	}
	public String getTrialManagerName() {
		return trialManagerName;
	}
	public void setTrialManagerName(String trialManagerName) {
		this.trialManagerName = trialManagerName;
	}
	public String getDesignTypeName() {
		return designTypeName;
	}
	public void setDesignTypeName(String designTypeName) {
		this.designTypeName = designTypeName;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public String getTrialTypeName() {
		return trialTypeName;
	}
	public void setTrialTypeName(String trialTypeName) {
		this.trialTypeName = trialTypeName;
	}
	public Integer getDesignTypeId() {
		return designTypeId;
	}
	public void setDesignTypeId(Integer designTypeId) {
		this.designTypeId = designTypeId;
	}
	public Integer getCurrentWorkflowId() {
		return currentWorkflowId;
	}
	public void setCurrentWorkflowId(Integer currentWorkflowId) {
		this.currentWorkflowId = currentWorkflowId;
	}
	public String getOwnGroupPermission() {
		return ownGroupPermission;
	}
	public void setOwnGroupPermission(String ownGroupPermission) {
		this.ownGroupPermission = ownGroupPermission;
	}
	public String getListTrialUnit() {
		return listTrialUnit;
	}
	public void setListTrialUnit(String listTrialUnit) {
		this.listTrialUnit = listTrialUnit;
	}
	public Integer getAccessGroupId() {
		return accessGroupId;
	}
	public void setAccessGroupId(Integer accessGroupId) {
		this.accessGroupId = accessGroupId;
	}
	public String getAccessGroupName() {
		return accessGroupName;
	}
	public void setAccessGroupName(String accessGroupName) {
		this.accessGroupName = accessGroupName;
	}
	public String getAccessGroupPermission() {
		return accessGroupPermission;
	}
	public void setAccessGroupPermission(String accessGroupPermission) {
		this.accessGroupPermission = accessGroupPermission;
	}
	public String getAccessGroupPerm() {
		return accessGroupPerm;
	}
	public void setAccessGroupPerm(String accessGroupPerm) {
		this.accessGroupPerm = accessGroupPerm;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public String getAddTrait() {
		return addTrait;
	}
	public void setAddTrait(String addTrait) {
		this.addTrait = addTrait;
	}
	public Integer getOwnGroupId() {
		return ownGroupId;
	}
	public void setOwnGroupId(Integer ownGroupId) {
		this.ownGroupId = ownGroupId;
	}
	public String getTriallocation() {
		return triallocation;
	}
	public void setTriallocation(String triallocation) {
		this.triallocation = triallocation;
	}
	public Integer getOwnGroupPerm() {
		return ownGroupPerm;
	}
	public void setOwnGroupPerm(Integer ownGroupPerm) {
		this.ownGroupPerm = ownGroupPerm;
	}
	public String getOwnGroupName() {
		return ownGroupName;
	}
	public void setOwnGroupName(String ownGroupName) {
		this.ownGroupName = ownGroupName;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public Integer getTrialNumber() {
		return trialNumber;
	}
	public void setTrialNumber(Integer trialNumber) {
		this.trialNumber = trialNumber;
	}
	public Integer getUltimatePerm() {
		return ultimatePerm;
	}
	public void setUltimatePerm(Integer ultimatePerm) {
		this.ultimatePerm = ultimatePerm;
	}
	public String getChgPerm() {
		return chgPerm;
	}
	public void setChgPerm(String chgPerm) {
		this.chgPerm = chgPerm;
	}
	public String getUltimatePermission() {
		return ultimatePermission;
	}
	public void setUltimatePermission(String ultimatePermission) {
		this.ultimatePermission = ultimatePermission;
	}
	public String getOtherPermission() {
		return otherPermission;
	}
	public void setOtherPermission(String otherPermission) {
		this.otherPermission = otherPermission;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public String getChgOwner() {
		return chgOwner;
	}
	public void setChgOwner(String chgOwner) {
		this.chgOwner = chgOwner;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getOtherPerm() {
		return otherPerm;
	}
	public void setOtherPerm(Integer otherPerm) {
		this.otherPerm = otherPerm;
	} 
	
	/*private String trialTitle;
	private Integer trialYear;
	
	public String getTrialName() {
		return trialName;
	}
	public void setTrialName(String trialName) {
		this.trialName = trialName;
	}
	public String getTrialTitle() {
		return trialTitle;
	}
	public void setTrialTitle(String trialTitle) {
		this.trialTitle = trialTitle;
	}
	public Integer getTrialYear() {
		return trialYear;
	}
	public void setTrialYear(Integer trialYear) {
		this.trialYear = trialYear;
	}*/

}
