package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Units;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class GeneralUnit implements ApiTO<Units>{
	
	
	private Integer  useBylayerattrib;
	private String   unitTypeName;
	private Integer  useByTrait;
	private Integer  useByTrialEvent;
	private Integer  useByItem;
	private Integer  unitTypeId;
	private String   update;
	private String   unitName;
	private String   unitNote;
	private String   unitSource;
	private Integer  unitId;
	
	public Integer getUseBylayerattrib() {
		return useBylayerattrib;
	}
	public void setUseBylayerattrib(Integer useBylayerattrib) {
		this.useBylayerattrib = useBylayerattrib;
	}
	public String getUnitTypeName() {
		return unitTypeName;
	}
	public void setUnitTypeName(String unitTypeName) {
		this.unitTypeName = unitTypeName;
	}
	public Integer getUseByTrait() {
		return useByTrait;
	}
	public void setUseByTrait(Integer useByTrait) {
		this.useByTrait = useByTrait;
	}
	public Integer getUseByTrialEvent() {
		return useByTrialEvent;
	}
	public void setUseByTrialEvent(Integer useByTrialEvent) {
		this.useByTrialEvent = useByTrialEvent;
	}
	public Integer getUseByItem() {
		return useByItem;
	}
	public void setUseByItem(Integer useByItem) {
		this.useByItem = useByItem;
	}
	public Integer getUnitTypeId() {
		return unitTypeId;
	}
	public void setUnitTypeId(Integer unitTypeId) {
		this.unitTypeId = unitTypeId;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getUnitNote() {
		return unitNote;
	}
	public void setUnitNote(String unitNote) {
		this.unitNote = unitNote;
	}
	public String getUnitSource() {
		return unitSource;
	}
	public void setUnitSource(String unitSource) {
		this.unitSource = unitSource;
	}
	public Integer getUnitId() {
		return unitId;
	}
	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

}
