package org.cimmyt.api.dal.to;

public class DALConstants {
	
	private DALConstants(){}
	
	
	public final static String ULTIMATE_PERMISSION = "Read/Write/Link";
	
	public final static String ACCESS_GROUP_PERMISSION = "Read/Link";
	
	public final static String OWN_GROUP_NAME = "admin";
	
	public final static String OWN_GROUP_PERMISSION = "Read/Write/Link";
	
	
	
}
