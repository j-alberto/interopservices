package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Variable;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class Trait implements ApiTO<Variable>{
	
	private String  OtherPermission;
	private String  traitUnitName;
	private String  traitValRuleErrMsg;
	private String  accessGroupName;
	private Integer accessGroupId;
	private Integer  traitGroupTypeId;
	private String  UltimatePermission;
	private String  traitDescription;
	private String  chgPerm;
	private String  accessGroupPermission;
	private String  ultimatePerm;
	private String  traitDataType;
	private String  accessGroupPerm;
	private String  ownGroupPermission;
	private String  update;
	private Integer traitUnit;
	private String  otherPerm;
	private String  ownGroupName;
	private String  ownGroupPerm;
	private Integer traitId;
	private String  traitValueMaxLength;
	private String  chgOwner;
	private String  traitCaption;
	private String  traitValRule;
	private String  addAlias;
	private String  traitName;
	private String  isTraitUsedForAnalysis;
	private String  ownGroupId;
	private String  delete;
	
	public String getOtherPermission() {
		return OtherPermission;
	}
	public void setOtherPermission(String otherPermission) {
		OtherPermission = otherPermission;
	}
	public String getTraitUnitName() {
		return traitUnitName;
	}
	public void setTraitUnitName(String traitUnitName) {
		this.traitUnitName = traitUnitName;
	}
	public String getTraitValRuleErrMsg() {
		return traitValRuleErrMsg;
	}
	public void setTraitValRuleErrMsg(String traitValRuleErrMsg) {
		this.traitValRuleErrMsg = traitValRuleErrMsg;
	}
	public String getAccessGroupName() {
		return accessGroupName;
	}
	public void setAccessGroupName(String accessGroupName) {
		this.accessGroupName = accessGroupName;
	}
	public Integer getAccessGroupId() {
		return accessGroupId;
	}
	public void setAccessGroupId(Integer accessGroupId) {
		this.accessGroupId = accessGroupId;
	}
	public Integer getTraitGroupTypeId() {
		return traitGroupTypeId;
	}
	public void setTraitGroupTypeId(Integer traitGroupTypeId) {
		this.traitGroupTypeId = traitGroupTypeId;
	}
	public String getUltimatePermission() {
		return UltimatePermission;
	}
	public void setUltimatePermission(String ultimatePermission) {
		UltimatePermission = ultimatePermission;
	}
	public String getTraitDescription() {
		return traitDescription;
	}
	public void setTraitDescription(String traitDescription) {
		this.traitDescription = traitDescription;
	}
	public String getChgPerm() {
		return chgPerm;
	}
	public void setChgPerm(String chgPerm) {
		this.chgPerm = chgPerm;
	}
	public String getAccessGroupPermission() {
		return accessGroupPermission;
	}
	public void setAccessGroupPermission(String accessGroupPermission) {
		this.accessGroupPermission = accessGroupPermission;
	}
	public String getUltimatePerm() {
		return ultimatePerm;
	}
	public void setUltimatePerm(String ultimatePerm) {
		this.ultimatePerm = ultimatePerm;
	}
	public String getTraitDataType() {
		return traitDataType;
	}
	public void setTraitDataType(String traitDataType) {
		this.traitDataType = traitDataType;
	}
	public String getAccessGroupPerm() {
		return accessGroupPerm;
	}
	public void setAccessGroupPerm(String accessGroupPerm) {
		this.accessGroupPerm = accessGroupPerm;
	}
	public String getOwnGroupPermission() {
		return ownGroupPermission;
	}
	public void setOwnGroupPermission(String ownGroupPermission) {
		this.ownGroupPermission = ownGroupPermission;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public Integer getTraitUnit() {
		return traitUnit;
	}
	public void setTraitUnit(Integer traitUnit) {
		this.traitUnit = traitUnit;
	}
	public String getOtherPerm() {
		return otherPerm;
	}
	public void setOtherPerm(String otherPerm) {
		this.otherPerm = otherPerm;
	}
	public String getOwnGroupName() {
		return ownGroupName;
	}
	public void setOwnGroupName(String ownGroupName) {
		this.ownGroupName = ownGroupName;
	}
	public String getOwnGroupPerm() {
		return ownGroupPerm;
	}
	public void setOwnGroupPerm(String ownGroupPerm) {
		this.ownGroupPerm = ownGroupPerm;
	}
	public Integer getTraitId() {
		return traitId;
	}
	public void setTraitId(Integer traitId) {
		this.traitId = traitId;
	}
	public String getTraitValueMaxLength() {
		return traitValueMaxLength;
	}
	public void setTraitValueMaxLength(String traitValueMaxLength) {
		this.traitValueMaxLength = traitValueMaxLength;
	}
	public String getChgOwner() {
		return chgOwner;
	}
	public void setChgOwner(String chgOwner) {
		this.chgOwner = chgOwner;
	}
	public String getTraitCaption() {
		return traitCaption;
	}
	public void setTraitCaption(String traitCaption) {
		this.traitCaption = traitCaption;
	}
	public String getTraitValRule() {
		return traitValRule;
	}
	public void setTraitValRule(String traitValRule) {
		this.traitValRule = traitValRule;
	}
	public String getAddAlias() {
		return addAlias;
	}
	public void setAddAlias(String addAlias) {
		this.addAlias = addAlias;
	}
	public String getTraitName() {
		return traitName;
	}
	public void setTraitName(String traitName) {
		this.traitName = traitName;
	}
	public String getIsTraitUsedForAnalysis() {
		return isTraitUsedForAnalysis;
	}
	public void setIsTraitUsedForAnalysis(String isTraitUsedForAnalysis) {
		this.isTraitUsedForAnalysis = isTraitUsedForAnalysis;
	}
	public String getOwnGroupId() {
		return ownGroupId;
	}
	public void setOwnGroupId(String ownGroupId) {
		this.ownGroupId = ownGroupId;
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	
	
	

}
