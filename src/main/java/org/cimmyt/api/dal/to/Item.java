package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.SeedStorage;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 *
 */

@JsonNaming(UpperCamelCaseStrategy.class)
public class Item implements ApiTO<SeedStorage> {
	
	private String  dateAdded;
	private String  specimenName;
	private String  itemSource;
	private Integer addedByUserId;
	private Integer itemTypeId;
	private Integer itemStateId;
	private String  update;
	private String  itemType;
	private Integer scaleId;
	private Integer itemSourceId;
	private String  storageLocation;
	private Integer trialUnitSpecimenId;
	private Integer unitId;
	private Integer itemId;
	private String  itemOperation;
	private Integer storageId;
	private String  deviceNote;
	private String  itemNote;
	private Integer specimenId;
	private String  lastMeasuredDate;
	private String  addParent;
	private Integer containerTypeId;
	private String  addedByUser;
	private String  delete;
	private String  itemState;
	private Double  amount;
	private Integer lastMeasuredUserId;
	private String  unitName;
	private String  itemBarcode;
	public String getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	public String getSpecimenName() {
		return specimenName;
	}
	public void setSpecimenName(String specimenName) {
		this.specimenName = specimenName;
	}
	public String getItemSource() {
		return itemSource;
	}
	public void setItemSource(String itemSource) {
		this.itemSource = itemSource;
	}
	public Integer getAddedByUserId() {
		return addedByUserId;
	}
	public void setAddedByUserId(Integer addedByUserId) {
		this.addedByUserId = addedByUserId;
	}
	public Integer getItemTypeId() {
		return itemTypeId;
	}
	public void setItemTypeId(Integer itemTypeId) {
		this.itemTypeId = itemTypeId;
	}
	public Integer getItemStateId() {
		return itemStateId;
	}
	public void setItemStateId(Integer itemStateId) {
		this.itemStateId = itemStateId;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public Integer getScaleId() {
		return scaleId;
	}
	public void setScaleId(Integer scaleId) {
		this.scaleId = scaleId;
	}
	public Integer getItemSourceId() {
		return itemSourceId;
	}
	public void setItemSourceId(Integer itemSourceId) {
		this.itemSourceId = itemSourceId;
	}
	public String getStorageLocation() {
		return storageLocation;
	}
	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}
	public Integer getTrialUnitSpecimenId() {
		return trialUnitSpecimenId;
	}
	public void setTrialUnitSpecimenId(Integer trialUnitSpecimenId) {
		this.trialUnitSpecimenId = trialUnitSpecimenId;
	}
	public Integer getUnitId() {
		return unitId;
	}
	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public String getItemOperation() {
		return itemOperation;
	}
	public void setItemOperation(String itemOperation) {
		this.itemOperation = itemOperation;
	}
	public Integer getStorageId() {
		return storageId;
	}
	public void setStorageId(Integer storageId) {
		this.storageId = storageId;
	}
	public String getDeviceNote() {
		return deviceNote;
	}
	public void setDeviceNote(String deviceNote) {
		this.deviceNote = deviceNote;
	}
	public String getItemNote() {
		return itemNote;
	}
	public void setItemNote(String itemNote) {
		this.itemNote = itemNote;
	}
	public Integer getSpecimenId() {
		return specimenId;
	}
	public void setSpecimenId(Integer specimenId) {
		this.specimenId = specimenId;
	}
	public String getLastMeasuredDate() {
		return lastMeasuredDate;
	}
	public void setLastMeasuredDate(String lastMeasuredDate) {
		this.lastMeasuredDate = lastMeasuredDate;
	}
	public String getAddParent() {
		return addParent;
	}
	public void setAddParent(String addParent) {
		this.addParent = addParent;
	}
	public Integer getContainerTypeId() {
		return containerTypeId;
	}
	public void setContainerTypeId(Integer containerTypeId) {
		this.containerTypeId = containerTypeId;
	}
	public String getAddedByUser() {
		return addedByUser;
	}
	public void setAddedByUser(String addedByUser) {
		this.addedByUser = addedByUser;
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	public String getItemState() {
		return itemState;
	}
	public void setItemState(String itemState) {
		this.itemState = itemState;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Integer getLastMeasuredUserId() {
		return lastMeasuredUserId;
	}
	public void setLastMeasuredUserId(Integer lastMeasuredUserId) {
		this.lastMeasuredUserId = lastMeasuredUserId;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getItemBarcode() {
		return itemBarcode;
	}
	public void setItemBarcode(String itemBarcode) {
		this.itemBarcode = itemBarcode;
	}	
	
	

}
