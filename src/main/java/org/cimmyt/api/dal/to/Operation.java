package org.cimmyt.api.dal.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.RestOperations;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 *
 */

@JsonNaming(UpperCamelCaseStrategy.class)
public class Operation implements ApiTO<RestOperations>{
	
	private String REST;

	public String getREST() {
		return REST;
	}

	public void setREST(String rEST) {
		REST = rEST;
	}
	
	
	
	

}
