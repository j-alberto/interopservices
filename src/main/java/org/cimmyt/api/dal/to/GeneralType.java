package org.cimmyt.api.dal.to;

import javax.persistence.Column;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.ScaleValue;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * 
 * @author RHTOLEDO
 *
 */
@JsonNaming(UpperCamelCaseStrategy.class)
public class GeneralType implements ApiTO<ScaleValue>{
	
	@Column(name="class")
	private String  Clase;
	private String  update;
	private Integer isTypeActive;
	private Integer typeId;
	private String  typeName;
	private String  typeNote;
	
	public Integer getIsTypeActive() {
		return isTypeActive;
	}
	public void setIsTypeActive(Integer isTypeActive) {
		this.isTypeActive = isTypeActive;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeNote() {
		return typeNote;
	}
	public void setTypeNote(String typeNote) {
		this.typeNote = typeNote;
	}
    
	public String getClase() {
		return Clase;
	}
	public void setClase(String clase) {
		Clase = clase;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
	
	
	

}
