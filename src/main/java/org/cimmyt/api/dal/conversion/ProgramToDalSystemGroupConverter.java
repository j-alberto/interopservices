package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.SystemGroup;
import org.cimmyt.model.Program;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Program} to DAL-specific TO
 * @author RHTOLEDO
 * @version 1.0
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class ProgramToDalSystemGroupConverter implements ApiConverter<Program>{

	@Override
	public ApiTO<Program> convert(Program source) {
		
		SystemGroup target = new SystemGroup();
		
		target.setSystemGroupId(source.getId());
		target.setSystemGroupName(source.getName());
		target.setSystemGroupDescription(source.getDescription());
		
		return target;
	}

}
