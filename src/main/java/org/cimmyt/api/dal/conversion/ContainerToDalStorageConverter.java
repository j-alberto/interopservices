package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.Storage;
import org.cimmyt.model.Container;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Container} to DAL-Specific TO
 * @author RHTOLEDO
 *
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class ContainerToDalStorageConverter implements ApiConverter<Container>{

	@Override
	public ApiTO<Container> convert(Container source) {

        Storage target = new Storage();
        
        target.setStorageId(source.getId());
        target.setStorageDetails(source.getName());
        target.setStorageNote(source.getLabel());
        target.setStorageBarcode("");
        target.setDelete("delete/storage/" + source.getId());
        target.setStorageLocation("Non existing");
        target.setUpdate("update/storage/" + source.getId());
        target.setStorageParentId(0);
		
		return target;
	}
	

}
