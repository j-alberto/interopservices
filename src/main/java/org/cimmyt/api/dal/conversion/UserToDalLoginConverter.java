package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.UserDal;
import org.cimmyt.model.User;
import org.springframework.stereotype.Component;

/**
 * Converts {@link User} to DAL-specific TO
 * @author RHTOLEDO
 *
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class UserToDalLoginConverter implements ApiConverter<User> {

	@Override
	public ApiTO<User> convert(User source) {
		
		UserDal target = new UserDal();
		target.setUserId(source.getId());

		return target;
	}

}
