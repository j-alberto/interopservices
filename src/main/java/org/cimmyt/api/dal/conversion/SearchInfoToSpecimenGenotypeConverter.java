package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.Specimen_Genotype;
import org.cimmyt.model.SearchInfo;
import org.springframework.stereotype.Component;

/**
 * Converts {@link SearchInfo} to DAL-specific TO
 * @author RHTOLEDO
 *
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class SearchInfoToSpecimenGenotypeConverter implements ApiConverter<SearchInfo>{

	@Override
	public ApiTO<SearchInfo> convert(SearchInfo source) {

		Specimen_Genotype  target = new Specimen_Genotype();
		
		target.setSolrId(source.getSolrId());
		target.setBreedingMethodId(source.getBreedingMethodId());
		target.setSpecimenId(source.getSpecimenId());
		target.setEntity_name(source.getEntityName());
		target.setSpecimenName(source.getSpecimenName());
		target.setGenotypeName(source.getGenotypeName());
		target.setFilialGeneration(source.getFilialGeneration());
		target.setGenotypeId(source.getGenotypeId());
		target.setIsActive(source.getIsActive());
		target.setGenusId(source.getGenusId());
		target.set_version_(source.getVersion());
		target.setSpecimenBarcode(source.getSpecimenBarcode());
		
		return target;
	}

}
