package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.Item;
import org.cimmyt.model.SeedStorage;
import org.springframework.stereotype.Component;

/**
 * Converts {@link SeedStorage} to DAL-specific TO
 * @author Raul Hernandez
 *
 */
@Component
@ApiConversion(ApiFormat.DAL)
public class SeedStorageToDalItemConverter implements ApiConverter<SeedStorage>{

	@Override
	public ApiTO<SeedStorage> convert(SeedStorage source) {
		
		Item target = new Item();
		
		target.setDateAdded("");
		
		target.setSpecimenId(source.getGid());
		target.setSpecimenName("");
		
		target.setItemId(source.getId());
		target.setItemSource("Add Item");
		
		target.setItemState("Seed - 0990613");
		target.setAmount(5.00);
		target.setLastMeasuredUserId(0);
		
		target.setItemTypeId(1);
		target.setItemType("");
		
		target.setItemStateId(1);
		target.setItemNote("");
		
		
		target.setScaleId(1);
		target.setItemSourceId(1);
		
		target.setStorageId(6);
		target.setStorageLocation("Non existing");
		
		target.setTrialUnitSpecimenId(1);
		
		target.setUnitId(11);
		target.setUnitName("U_1442236");
		
		target.setItemOperation("group");
		
		target.setDeviceNote("");
		
		
		target.setAddedByUserId(1);
		target.setUpdate("updte/item/"+source.getId());
		target.setAddParent("item/"+source.getId()+"/add/parent");
		target.setDelete("delete/item/" + source.getId());
		target.setAddedByUser("admin");
		
		target.setLastMeasuredDate("");
		
		target.setContainerTypeId(132);
		
		
		
		
		target.setItemBarcode("");
		
		return target;
	}

}
