package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.BreedingMethod;
import org.cimmyt.model.BreedingMethodVariable;
import org.springframework.stereotype.Component;


/**
 * Converts {@link BreedingMethodVariable} to DAL-Specific TO
 * @author RHTOLEDO
 *
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class BrVariableToDalBrMethodConverter implements ApiConverter<BreedingMethodVariable>{

	@Override
	public ApiTO<BreedingMethodVariable> convert(BreedingMethodVariable source) {
		
		BreedingMethod target = new BreedingMethod();
		
		target.setUpdate("update/breedingmethod/" + source.getId());
		target.setBreedingMethodId(source.getId());
		target.setBreedingMethodNote(source.getDescription());
		target.setBreedingMethodName(source.getName());
		
		return target;
	}

}
