package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.InstanceNumber;
import org.cimmyt.model.PlotData;
import org.springframework.stereotype.Component;

/**
 * Converts {@link PlotData} to DAL-specific TO
 * @author RHTOLEDO
 *
 */
@Component
@ApiConversion(ApiFormat.DAL)
public class PlotDataToDalInstanceConverter implements ApiConverter<PlotData>{

	@Override
	public ApiTO<PlotData> convert(PlotData source) {
		
		InstanceNumber target = new InstanceNumber();
		
		target.setTraitId(source.getVariable().getId());
		target.setInstanceNumber(1);
		target.setSampleTypeId(1);
		
		return target;
	}
}
