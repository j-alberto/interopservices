package org.cimmyt.api.dal.conversion;

import java.util.Optional;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.Site;
import org.cimmyt.model.Geolocation;
import org.cimmyt.model.Place;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Place} to DAL-specific TO
 * @author RHTOLEDO
 * @version 1.0
 * 
 * Here a DAL Example taken from kddart.org:
 * 
      {
         "Latitude" : "-35.33909",
         "sitelocation" : "POLYGON((148.99658 -35.48192,149.2067 -35.48192,149.2067 -35.19626,148.99658 -35.19626,148.99658 -35.48192))",
         "update" : "update/site/16",
         "SiteTypeName" : "SiteType - 5695208",
         "SiteId" : "16",
         "CurrentSiteManagerId" : "18",
         "SiteEndDate" : "0000-00-00 00:00:00",
         "SiteAcronym" : "GH",
         "SiteStartDate" : "0000-00-00 00:00:00",
         "CurrentSiteManagerName" : "Testing User-8788499",
         "SiteTypeId" : "98",
         "SiteName" : "DArT Test Site",
         "Longitude" : "149.10164"
      } 
 * 
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class PlaceToDalSiteConverter implements ApiConverter<Place>{

	@Override
	public ApiTO<Place> convert(Place source) {
		
		Site target = new Site();
		
		target.setSiteId(source.getId());
		target.setSiteName(source.getName());
   
		target.setSitelocation("");
		target.setSiteAcronym(source.getAbbrev());		
		
		target.setSiteTypeId("");
		target.setSiteTypeName(source.getPlaceType());
		
		target.setCurrentSiteManagerId(null);
		target.setCurrentSiteManagerName(null);
		
		target.setSiteStartDate(null);
		target.setSiteEndDate(null);
		
		Optional<Geolocation> geo = Optional.ofNullable(source.getGeolocation());
		
		target.setLatitude(geo.orElse(new Geolocation()).getLatitude());
		target.setLongitude(geo.orElse(new Geolocation()).getLongitude());
		
		target.setUpdate("update/site/" + source.getId());
		
		return target;
	}

}
