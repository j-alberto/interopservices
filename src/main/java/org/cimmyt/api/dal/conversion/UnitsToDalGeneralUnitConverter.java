package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.GeneralUnit;
import org.cimmyt.model.Units;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Units} to DAL-specific TO
 * @author RHTOLEDO
 * @version 1.0
 *
 */
@Component
@ApiConversion(ApiFormat.DAL)
public class UnitsToDalGeneralUnitConverter implements ApiConverter<Units>{

	@Override
	public ApiTO<Units> convert(Units source) {
		
		GeneralUnit target = new GeneralUnit();
		
		target.setUseBylayerattrib(0);
		target.setUnitTypeName("none");
		target.setUseByTrait(0);
		target.setUseByTrialEvent(0);
		target.setUseByItem(0);
		target.setUnitTypeId(source.getType());
		target.setUpdate("update/generalunit/" + source.getId());
		target.setUnitName(source.getName());
		target.setUnitNote("none");
		target.setUnitSource("none");
		target.setUnitId(source.getId());
		
		return target;
	}

}
