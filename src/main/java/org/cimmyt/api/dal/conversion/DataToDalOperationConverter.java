package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.Operation;
import org.cimmyt.model.RestOperations;
import org.springframework.stereotype.Component;

/**
 * Converts {@link RestOperations} to DAL-specific TO
 * @author RHTOLEDO
 *
 */
@Component
@ApiConversion(ApiFormat.DAL)
public class DataToDalOperationConverter implements ApiConverter<RestOperations>{

	@Override
	public ApiTO<RestOperations> convert(RestOperations source) {

       Operation target = new Operation();
       target.setREST(source.getDalRestOperation());
		
   	   return target;
	}

}
