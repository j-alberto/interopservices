package org.cimmyt.api.dal.conversion;

import java.util.List;
import java.util.Optional;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.Trial;
import org.cimmyt.model.B4RVariables;
import org.cimmyt.model.Geolocation;
import org.cimmyt.model.Place;
import org.cimmyt.model.Study;
import org.cimmyt.model.StudyMetadata;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Study} to DAL-specific TO
 * @author jarojas
 *
 */
@Component
@ApiConversion(ApiFormat.DAL)
class StudyToDalTrialConverter implements ApiConverter<Study> {

	@Override
	public ApiTO<Study> convert(Study source) {
		Trial target = new Trial();
		
		List<StudyMetadata> studyMetadata = source.getStudyMetadata(); 
		
		target.setProjectId(source.getId());
		target.setProjectName(B4RVariables.getValueFromMetadata(studyMetadata, B4RVariables.STUDY_NAME));
		
		target.setTrialId(source.getId());
		target.setTrialNumber(source.getId());
		
		target.setTrialName(source.getTitle());
		target.setTrialNote(B4RVariables.getValueFromMetadata(studyMetadata, B4RVariables.STUDY_REMARKS));
		
		target.setTrialAcronym(source.getName());
		
		target.setTrialStartDate(null);
		target.setTrialEndDate(null);
		
		target.setTrialTypeId(null);
		target.setTrialTypeName(B4RVariables.getValueFromMetadata(studyMetadata, B4RVariables.STUDY_TYPE));
		
		target.setTrialManagerId(null);
		target.setTrialManagerName(null);
		
		target.setDesignTypeId(null);
		target.setDesignTypeName(B4RVariables.getValueFromMetadata(studyMetadata, B4RVariables.EXPERIMENTAL_DESIGN));
		
		target.setCurrentWorkflowId(new Integer(0));
		target.setOwnGroupPermission("");
		target.setListTrialUnit("trial/" + source.getId() +"/list/trialunit");
		
		target.setAccessGroupId(new Integer(0));
		target.setAccessGroupName("");
		target.setAccessGroupPermission("");
		target.setAccessGroupPerm("");
		
		target.setAddTrait("trial/" + source.getId() + "/add/trait");
		target.setOwnGroupId(new Integer(0));
		
		//Example : "POLYGON((149.092666904502 -35.3047819041308,149.092784921705 -35.3047819041308,149.09276882845 
		//                    -35.3049745290585,149.092650811256 -35.3049745290585,149.092666904502 -35.3047819041308))"
		target.setTriallocation("");
		
		target.setOwnGroupPerm(new Integer(0));
		target.setOwnGroupName("");
		
		Optional<Place> place = Optional.ofNullable(source.getPlace());
		target.setSiteId(place.orElse(new Place()).getId());
		target.setSiteName(place.orElse(new Place()).getName());
		
		Optional<Geolocation> geo = Optional.ofNullable(source.getPlace().getGeolocation());
		target.setLongitude(geo.orElse(new Geolocation()).getLongitude());
		target.setLatitude(geo.orElse(new Geolocation()).getLatitude());
		
		target.setMap("trial/"+ source.getId() +"/on/map");
		
		target.setUpdate("update/trial/" + source.getId());
		target.setUltimatePerm(new Integer(0));
		target.setUltimatePermission("Read/Write/Link");
		target.setOtherPermission("Read/Link");
		target.setChgPerm("trial/" + source.getId() + "/change/permission");
		target.setChgOwner(null);
		target.setOtherPerm(null);

		
		return target;
	}
}
