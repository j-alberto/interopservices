package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.TrialTrait;
import org.cimmyt.model.Variable;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Variable} to DAL-specific TO 
 * @author RHTOLEDO
 *
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class VariableToDalTraitConverter implements ApiConverter<Variable> {

	private Integer count;
	
	public VariableToDalTraitConverter(){
		this.count = 0;
	}
	
	@Override
	public ApiTO<Variable> convert(Variable source) {
		
		/*Trait target = new Trait();
		
		target.setTraitId(source.getId());
		target.setTraitName(source.getName());
		target.setTraitDescription(source.getDescription());
		
		//TODO mandar fijo para que coincida con el catalogo, fix it
		//target.setTraitUnit(source.getScale().getId());
		target.setTraitUnit(24);
		
		//TODO por que viene null? fix it
		//target.setTraitUnitName(source.getScale().getName());
		target.setTraitUnitName("U_" + source.getId());
		
		target.setTraitValueMaxLength("20");
		target.setTraitValRuleErrMsg("invalid value");
		
		//TODO se convierte a mayusculas, fix it
		target.setTraitDataType(source.getDataType().toUpperCase());
		target.setTraitCaption(source.getDisplayName());
		
		//TODO ver de donde se va a sacar la regla de validacion
		//target.setTraitValRule(source.getScale().getScaleValue());
		target.setTraitValRule("RANGE(40..180)");
		
		target.setIsTraitUsedForAnalysis("1");
		target.setTraitGroupTypeId(0);
		
		target.setAccessGroupId(0);
		target.setAccessGroupName(DALConstants.OWN_GROUP_NAME);
		target.setAccessGroupPerm("5");
		target.setAccessGroupPermission(DALConstants.ACCESS_GROUP_PERMISSION);
		
		target.setUltimatePermission(DALConstants.ULTIMATE_PERMISSION);
		
		target.setUltimatePerm("7");
		target.setOtherPerm("0");
		target.setOtherPermission("none");
		
		target.setOwnGroupPermission(DALConstants.OWN_GROUP_PERMISSION);
		target.setOwnGroupId("0");
		target.setOwnGroupName(DALConstants.OWN_GROUP_NAME);
		target.setOwnGroupPerm("7");
		
		target.setChgOwner("trait/" + source.getId() + "/change/owner");
		target.setAddAlias("trait/" + source.getId() + "" );
        target.setDelete("delete/trait/" + source.getId());
		target.setUpdate("update/trait/" + source.getId());
		target.setChgPerm("trait/" + source.getId() + "/change/permision");
		
		return target;*/
		
		//TODO Fix all hard code
		TrialTrait target = new TrialTrait();
		
		target.setTraitName(source.getName());
		target.setTraitValRule("RANGE(40..180)");
		target.setCompulsory(1);
		target.setTrialTraitId(++count);
		target.setDelete("trial/1/remove/trait/1");
		target.setTraitId(source.getId());
		target.setUpdate("update/trialtrait/5");
		target.setTraitUnit(24);
		target.setTraitValueMaxLength(20);
		target.setTrialId(34055);
		target.setTraitDataType("INTEGER");
		
		return target;
		
	}
}
