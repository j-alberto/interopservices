package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.GeneralType;
import org.cimmyt.model.B4RVariables;
import org.cimmyt.model.ScaleValue;
import org.springframework.stereotype.Component;

/**
 * Converts {@link ScaleValue} to DAL-specific TO
 * @author RHTOLEDO
 *
 */
@Component
@ApiConversion(ApiFormat.DAL)
public class ScaleToDalTrialTypeConverter implements ApiConverter<ScaleValue>{

	@Override
	public ApiTO<ScaleValue> convert(ScaleValue source) {
		
		GeneralType target = new GeneralType();
		
		target.setTypeId(source.getId());
		target.setTypeName(source.getDisplayName());
		target.setTypeNote(source.getDescription());
		target.setIsTypeActive(B4RVariables.isActiveFromIsVoid(source.getIsVoid()));
		target.setUpdate("update/type/trial/" + source.getId());
		target.setClase("trial");
		
		return target;
	}
	
}
