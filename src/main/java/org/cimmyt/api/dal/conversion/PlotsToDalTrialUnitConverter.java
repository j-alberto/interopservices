package org.cimmyt.api.dal.conversion;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.api.dal.to.DALConstants;
import org.cimmyt.api.dal.to.Specimen;
import org.cimmyt.api.dal.to.TrialUnit;
import org.cimmyt.model.Plot;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Plot} to DAL-specific TO
 * @author RHTOLEDO
 *
 */

@Component
@ApiConversion(ApiFormat.DAL)
public class PlotsToDalTrialUnitConverter implements ApiConverter<Plot>{

	@Override
	public ApiTO<Plot> convert(Plot source) {
		
		Integer trialUnitId = source.getPlotno();
		
		TrialUnit target = new TrialUnit();
		
		target.setTrialId(source.getStudy().getId());
		
		target.setTrialUnitBarcode("");
		target.setSourceTrialUnitId(0);
		target.setTrialUnitId(trialUnitId);
		
		/**
		 * No data in B4R to map
		 * Longitude or latitud a plot level ?
		 */
		target.setLongitude(null);
		target.setLatitude(null);
		
		target.setReplicateNumber(source.getRep());
		target.setTrialUnitNote("");
		
		target.setTreatmentId(null);
		target.setTreatmentText("");
		
		target.setSiteId(source.getStudy().getPlace().getId());
		target.setSiteName(source.getStudy().getPlace().getName());
		
		target.setUnitPositionId(null);
		target.setSampleSupplierId(null);
		
		target.setUnitPositionText("");
		target.setTrialUnitX(1);
		//target.setTrialUnitY(1);
		target.setTrialUnitY(trialUnitId);
		target.setTrialUnitZ(0);
		
		target.setTrialUnitPosition("Entry " + source.getPlotno());
		//target.setTrialUnitEntryId(1);
		target.setTrialUnitEntryId(trialUnitId);
		
		
		Specimen specimen = new Specimen();
		specimen.setNotes("");
		specimen.setTrialUnitId(trialUnitId);
		specimen.setItemId("");
		specimen.setHasDied("");
		specimen.setPlantDate("");
		specimen.setTrialUnitSpecimenId(trialUnitId);
		specimen.setHarvestDate("");
		specimen.setSpecimenId(source.getEntry().getProductGid());
		specimen.setSpecimenName(source.getEntry().getProduct().getDesignation());
		
		target.setSpecimen(specimen);
		
		target.setUltimatePerm(null);
		target.setAddSpecimen("trialunit/"+ trialUnitId +"/add/specimen");
		target.setUpdate("update/trialunit/" + trialUnitId );
		target.setListSpecimen("trialunit/"+ trialUnitId +"/list/specimen");		
		target.setAddKeyword("trialunit/"+ trialUnitId +"/add/keyword");
		target.setUltimatePermission(DALConstants.ULTIMATE_PERMISSION);
		
		return target;
	}
	
	

}
