package org.cimmyt.api.error;

import org.cimmyt.api.ApiResponse;

/**
 * Defines the body of responses for NOT_FOUND errors. Allows different APIs to define their own
 * response format. 
 * @author jarojas
 *
 */
interface MappingError {

	ApiResponse<String> getBody(String errorMessge);
}
