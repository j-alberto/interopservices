package org.cimmyt.api.error;

import org.cimmyt.api.brapi.call.BrResponseBuilder;
import org.cimmyt.api.brapi.call.BrapiResponse;
import org.cimmyt.api.conversion.ApiConversion;
import org.springframework.stereotype.Component;

/**
 * NOT_FOUND Error format for BrAPI calls
 * @author jarojas
 *
 */
@Component
@ApiConversion
class BrMappingError implements MappingError{

	@Override
	public BrapiResponse<String> getBody(String errorMessge) {
		return BrResponseBuilder.forError(errorMessge);
	}

}
