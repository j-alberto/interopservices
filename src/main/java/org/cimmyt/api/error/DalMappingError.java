package org.cimmyt.api.error;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiResponse;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.dal.call.DalResponseBuilder;
import org.springframework.stereotype.Component;

/**
 * NOT_FOUND Error format for DAL calls
 * @author jarojas
 */
@Component
@ApiConversion(ApiFormat.DAL)
class DalMappingError implements MappingError{

	@Override
	public ApiResponse<String> getBody(String errorMessge) {
		return DalResponseBuilder.forError(errorMessge);
	}

}
