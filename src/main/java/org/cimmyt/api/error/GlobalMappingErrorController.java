package org.cimmyt.api.error;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.cimmyt.api.ApiFormat;
import org.cimmyt.api.ApiResponse;
import org.cimmyt.api.conversion.ApiConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Returns a NOT_FOUND error message when the requested resource cannot be found in any Resource class for any API, that is when a request cannot be mapped to a Resource's method.
 * @author jarojas
 *
 */
@RestController
class GlobalMappingErrorController extends AbstractErrorController {
	private static final String ERROR_MAPPING = "/error";

	@Autowired
	private List<MappingError> mappings;

	private final Map<ApiFormat, MappingError> mapErrorFormats = new HashMap<>();

	public GlobalMappingErrorController(ErrorAttributes errorAttributes) {
		super(errorAttributes);
	}


	/**
	 * Initialize the map containing error mappers for APIs. The key is a {@link APIFormat} and the value its converter
	 */
	@PostConstruct
	public void init() {
		mappings.forEach(mapElem -> mapErrorFormats.put(
				((ApiConversion) (AnnotationUtils.findAnnotation(mapElem.getClass(), ApiConversion.class))).value(),
				mapElem));
	}

	/**
	 * Tries to build a NOT_FOUND error message, including a body for the proper API
	 * @param request that originated the NOT_FOUND error
	 * @return a NOT_FOUND response body format suitable for the request, or without body if the APIFormat cannot be inferred from request 
	 */
	@RequestMapping(value = "/error")
	public ResponseEntity<ApiResponse<String>> error(HttpServletRequest request) {
		ApiFormat api = null;
		ApiResponse<String> body;
		try {
			String path = super.getErrorAttributes(request, false).get("path").toString().split("/")[1].toUpperCase();
			api = ApiFormat.valueOf(path);
			body = mapErrorFormats.get(api).getBody("resource not found");
		} catch (Exception ex) {
			body = null;
		}

		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}

	@Override
	public String getErrorPath() {
		return ERROR_MAPPING;
	}


	public void setMappings(List<MappingError> mappings) {
		this.mappings = mappings;
	}
}
