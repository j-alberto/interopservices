package org.cimmyt.api.brapi.conversion;

import java.util.Optional;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.brapi.to.BrLocationTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.model.Country;
import org.cimmyt.model.Geolocation;
import org.cimmyt.model.Place;
import org.springframework.stereotype.Component;

/**
 * Converts {@link Place} to BrAPI-specific TO
 * @author RHTOLEDO
 * @version 1.0
 */

@Component
@ApiConversion
public class PlaceToBrLocationConverter implements ApiConverter<Place> {

	@Override
	public ApiTO<Place> convert(Place source) {

        BrLocationTO target = new BrLocationTO();
        
        target.setLocationDbId(source.getId());
        target.setLocationType(source.getPlaceType());
        target.setName(source.getName());
        target.setAbbreviation(source.getAbbrev());
        
        Optional<Geolocation> geo = Optional.ofNullable(source.getGeolocation());
        target.setLatitude(geo.orElse(new Geolocation()).getLatitude());
        target.setLongitude(geo.orElse(new Geolocation()).getLongitude());
        target.setAltitude(geo.orElse(new Geolocation()).getElevation());
        
        Optional<Country> country = Optional.empty();
        
        if(geo.isPresent()){
           country = Optional.ofNullable(source.getGeolocation().getCountry());
        }
        
        target.setCountryCode(country.orElse(new Country()).getIsoCode());
        target.setCountryName(country.orElse(new Country()).getName());
        

        
		return target;
	}
	
	
	
	

}
