package org.cimmyt.api.brapi.conversion;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.brapi.to.BrStudyTO;
import org.cimmyt.api.conversion.ApiConversion;
import org.cimmyt.api.conversion.ApiConverter;
import org.cimmyt.model.Study;
import org.springframework.stereotype.Component;
/**
 * Converts {@link Study} to BrAPI-specific TO
 * @author jarojas
 *
 */
@Component
@ApiConversion
class StudyToBrStudyConverter implements ApiConverter<Study> {

	@Override
	public ApiTO<Study> convert(Study source) {
		BrStudyTO target = new BrStudyTO();
		
		target.setTitle(source.getTitle());
		target.setaYear(source.getYear());
		target.setName(source.getName()+"==>"+source.getNumber());
		
		return target;
	}
}
