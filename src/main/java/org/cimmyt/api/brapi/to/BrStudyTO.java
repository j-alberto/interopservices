package org.cimmyt.api.brapi.to;

import org.cimmyt.api.ApiTO;
import org.cimmyt.model.Study;

/**
 * {@link ApiTO Transfer Object} for {@link Study} in BrAPI calls
 * @author jarojas
 *
 */
public class BrStudyTO implements ApiTO<Study>{
	
	private String name;
	private String title;
	private Integer aYear;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getaYear() {
		return aYear;
	}
	public void setaYear(Integer aYear) {
		this.aYear = aYear;
	}

	
}
