package org.cimmyt.api.brapi.resource;

import java.util.List;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.brapi.call.BrOperationServices;
import org.cimmyt.api.brapi.call.BrResponseBuilder;
import org.cimmyt.api.brapi.call.BrapiResponse;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.model.Program;
import org.cimmyt.service.SystemCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in BrAPI format for {@link Program} entity,
 * acconding to BrAPI V1 definition:
 * 
 * Parameters: programName   - optional
 *             abbrevitation - optional
 * 
 * Pagination: Yes
 * 
 * @author RHTOLEDO
 * @version 1.0 
 */
@RestController
@RequestMapping(value="/brapi/v1/programs",  method=RequestMethod.GET)
public class BrProgramResource implements BrOperationServices<Program>{
	
	private ApiConversionService<Program> programConverter;
	private SystemCatalogService catalogService;
	
		
	@Autowired
	public BrProgramResource(ApiConversionService<Program> programConverter,
			SystemCatalogService catalogService){
		super();
		this.programConverter = programConverter;
		this.catalogService   = catalogService;
	}
	
	@Override
	@RequestMapping(method=RequestMethod.GET)
    public BrapiResponse<List<ApiTO<Program>>> findAll(
    	    		@PageableDefault(page=0,size=50) Pageable pageRequest){
		
    	Page<ApiTO<Program>> page = programConverter.convert(
    			catalogService.getPrograms(pageRequest));
    	
    	return BrResponseBuilder.forData(page.getContent())
    			.addStatusSuccess()
    			.withPagination(page)
    			.build();
    	
    } 
}
