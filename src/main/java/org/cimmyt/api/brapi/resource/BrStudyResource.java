package org.cimmyt.api.brapi.resource;

import java.util.List;

import org.cimmyt.api.brapi.call.BrapiResponse;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.api.ApiTO;
import org.cimmyt.api.brapi.call.BrResponseBuilder;
import org.cimmyt.model.Study;
import org.cimmyt.service.StudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in BrAPI format for {@link Study} entities
 * @author jarojas
 *
 */
@RestController
@RequestMapping(value="/brapi/study", method=RequestMethod.GET)
public class BrStudyResource {

	private ApiConversionService<Study> studyConverter;
	private StudyService studyService;
	
	@Autowired
	public BrStudyResource(ApiConversionService<Study> studyConverter,
			StudyService studyService) {
		super();
		this.studyConverter = studyConverter;
		this.studyService = studyService;
	}
	
	/**
	 * Finds a study TOs. Supports pagination.
	 * @param pageRequest page of the study dataset. Defaults to first page with 50 elements
	 * @return the page requested
	 */
	@RequestMapping(method=RequestMethod.GET)
	public BrapiResponse<List<ApiTO<Study>>> findStudies(
			@PageableDefault(page=0,size=50)Pageable pageRequest) {
		
		Page<ApiTO<Study>> page = studyConverter.convert(
				studyService.getStudies(pageRequest));

		return BrResponseBuilder.forData(page.getContent())
				.addStatusSuccess()
				.withPagination(page)
				.build();
	}
	
	
}
