package org.cimmyt.api.brapi.resource;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.cimmyt.api.brapi.call.BrResponseBuilder;
import org.cimmyt.api.brapi.call.BrOperationServices;
import org.cimmyt.api.brapi.call.BrapiResponse;
import org.cimmyt.api.conversion.ApiConversionService;
import org.cimmyt.model.Place;
import org.cimmyt.service.SystemCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Resource in BrAPI format for {@link Place} entity,
 * according to BrAPI V1 definition
 * 
 * @author RHTOLEDO
 * @version 1.0
 */



@RestController
@RequestMapping(value="/brapi/v1/locations",  method=RequestMethod.GET)
public class BrLocationResource implements BrOperationServices<Place>{
	
	private ApiConversionService<Place> placeConverter;
	private SystemCatalogService        locationService;
	
	@Autowired
	public BrLocationResource(ApiConversionService<Place> placeConverter,
			                  SystemCatalogService locationService){
		super();
		this.placeConverter  = placeConverter;
		this.locationService = locationService;		
		
	}
	
	@Override
	@RequestMapping(method=RequestMethod.GET)
	public BrapiResponse<List<ApiTO<Place>>> findAll(
			  @PageableDefault(page=0,size=50) Pageable pageRequest){
		
		Page<ApiTO<Place>> page = placeConverter.convert(
				  locationService.getLocations(pageRequest));		
		
    	return BrResponseBuilder.forData(page.getContent())
    			.addStatusSuccess()
    			.withPagination(page)
    			.build();
		
	}	
}
