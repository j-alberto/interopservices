package org.cimmyt.api.brapi.call;

import java.util.List;

import org.cimmyt.api.ApiTO;
import org.springframework.data.domain.Pageable;

public interface BrOperationServices<T> {
	
	public BrapiResponse<List<ApiTO<T>>> findAll(Pageable pageRequest);

}
