/*
 * BMSAPI - BrAPI expansion
 * CIMMYT, 2016
 * License pending
 */
package org.cimmyt.api.brapi.call;

import org.cimmyt.api.ApiResponse;

/**
 * Global format for any BrAPI call
 * @author jarojas
 *
 * @param <T> the payload type in this response
 */
//TODO reduce T to ApiTO
public class BrapiResponse<T> implements ApiResponse<T>{

	private BrMetadata metadata;
	/**
	 * Contains the payload
	 */
	private T result;
	
	public BrapiResponse() {
		metadata = new BrMetadata();
	}
	
	public BrMetadata getMetadata() {
		return metadata;
	}
	public void setMetadata(BrMetadata metadata) {
		this.metadata = metadata;
	}
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	
}
