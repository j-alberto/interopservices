/*
 * BMSAPI - BrAPI expansion
 * CIMMYT, 2016
 * License pending
 */
package org.cimmyt.api.brapi.call;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Singleton-fluent builder for responses that comply with BrAPI standards.
 * @author jarojas
 * @category utility
 */
//TODO create forPagedData. Remove withPagination in Response inner class
public final class BrResponseBuilder {
	
	private static BrResponseBuilder responseBuilder = new BrResponseBuilder();
	
	private BrResponseBuilder(){}

	/**
	 * Builder's entry point
	 * @param payload to add in the response
	 * @return intermediate {@link Response} for further configuration
	 */
	public static <T>Response<T> forData(T payload){
		return responseBuilder.new Response<T>(payload);
	}

	/**
	 * Builder's entry and end point for creating responses describing an error
	 * @param errorMessage to display in metadata
	 * @return BrAPI compilant error response
	 */
	public static BrapiResponse<String> forError(String errorMessage){
		return responseBuilder.new Response<String>("")
				.withNoPagination()
				.addStatusError(errorMessage)
				.build();
	}
	
	/**
	 * Inner Class with calls for custom addition of elements in a response. Enforces the fluent
	 * use of the builder
	 * @author jarojas
	 *
	 * @param <T> the payload's class
	 */
	public class Response<T>{
		BrapiResponse<T> response;
		
		private Response(T payload){
			this.response = new BrapiResponse<>();
			this.response.setResult(payload);
			response.setMetadata(new BrMetadata());
		}
		
		public Response<T> withNoPagination(){
			return withPagination(emptyPage());
		}
				
		public Response<T> withPagination(Page<?> page){
			response.getMetadata().setPagination(
					new BrPagination(page.getSize(),
							page.getNumber(),
							page.getTotalElements(),
							page.getTotalPages()));
			return this;
		}
		
		private Page<?> emptyPage(){
			return new EmptyPage(Collections.emptyList());
		}
		
		/**
		 * Exit point of the builder.
		 * @return the {@link BrapiResponse} built
		 */
		public BrapiResponse<T> build(){
			return response;
		}
		
		public Response<T> addStatusSuccess(){
			addStatus(BrStatus.SUCCESS);
			return this;
		}
		public Response<T> addStatusError(){
			addStatus(BrStatus.ERROR);
			return this;
		}
		public Response<T> addStatusError(String message){
			addStatus(new BrStatus(BrStatus.ERROR_CODE, message));
			return this;
		}
		
		private void addStatus(BrStatus status){
			if(response.getMetadata().getStatus() == null){
				response.getMetadata().setStatus(new ArrayList<BrStatus>());
			}
			response.getMetadata().getStatus().add(status);			
		}
		
		class EmptyPage extends PageImpl<T>{
			private static final long serialVersionUID = 2L;

			public EmptyPage(List<T> content) {	super(content);	}
		
			/**
			 * Brapi requires total pages to be 0 for no-pagination metadata
			 */
			@Override
			public int getTotalPages() {
				return 0;
			}
			
		}
		
	}
}
