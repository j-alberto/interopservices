# Inter Operability Services (Hermes)
This project aims to provide services to exchange information through web services to multiple platforms, for example a B4R database providing information to KDTools and/or GrinGlobal. Currently it supports B4R database.

The application provides a flexible environment for publishing RESTful services using different specifications, for example KDDart/DAL API or Breeding API.

## Requirements
* The project requires JDK and JRE 8.
* Available port for deploying (preset is 9080). If the preset port is not available in the host machine, can be changed in application's configuration.
* A B4R DB instance with all schemas

## Build
You can compile and build the project by using either Maven or Gradle. The project creates both an executable _jar_ file and a clean _jar_ with only the application classes.

### Considerations
* Build pipelines for Maven and Gradle have subtle differences which must be considered when changing provided configuration. Please refer to their documentation for further information.
* For both systems it is recommended to add the task/goal 'clean' to avoid some caching and force a full compilation cycle. Do this specially when building a release package.

### Gradle
```
$ gradle bootRepackage
```
This will create the file _hermes-{version}.jar_ under {projectBaseDir}/build/libs/

To explicitly run tests before packaging add the task 'test'
```
$ gradle test bootRepackage
```

### Maven

```
$ mvn package
```
This will create the file _hermes-{version}.jar_ under {projectBaseDir}/target/

In maven's build cycle the goal 'test' is executed by default, to disable it add the argument **-DskipTests** 


## Run
Simply run it as a regular java executable:
```
$ java -jar hermes-{version}.jar
```
You can specify a server port other than 9080 as:
```
$ java -jar hermes-{version}.jar --server.port=8080
```
Running the application as a _war_ file inside a web application server will be available in future releases.

## Development run
To run the application directly from code:

### Gradle
```
$ gradle bootRun
```
### Maven
```
$ mvn spring-boot:run
```

In both cases press **CTRL+C** to stop execution.

## Testing
Peace of cake

### Considerations
* Despite both tools generate test results (xml, text summaries, etc), only Gradle provides a human-readable report in html.
* Integration tests require a B4R database with very specific CIMMYT's data loaded in it. Consider disabling these tests if you don't have a copy of this DB for testing.

### Gradle
Run task 'test', and optionally 'jacocoTestReport' to generate a code coverage report:
```
$ gradle test jacocoTestReport
```
* Report of test execution is in {projectBaseDir}/build/reports/tests/index.html
* Report of code coverage is in {projectBaseDir}/build/reports/jacoco/test/html/index.html

### Maven
Run goal 'test', and optionally 'jacoco:report' to generate a code coverage report:
```
$ mvn test jacoco:report
```
* Test execution results are under {projectBaseDir}/target/surefire-reports/
* Report of code coverage is in {projectBaseDir}/target/site/jacoco/index.html

##Debug
The easiest way to debug is configuring eclipse for a remote application. 
  - a) Go to: Run > Debug Configurations > Remote Java Application
  - b) Add a new Configuration
  - c) Select the project
  - d) Set the port to 5005. (Change the host name if your application runs in a different host)
  - e) Go to: Run > Debug Configurations > Maven Build
  - f) Add a new Maven Build
  - g) Select the project and fill Goals with spring-boot:run
  - h) Add a new parameter > Parameter Name: debug Value: true
   
  - Add breakpoints as needed

### Considerations
* Debugging with a IDE is most of the time the easyest way. Current documentation focuses on Eclipse and Eclipse-based IDEs.
* Maven configuration supports debugging of application code. Debugging of tests will be added soon.

After configuring eclipse, run with the proper argument:

### Gradle
```
$ gradle bootRun -Ddebug=true
$ gradle test -Ddebug=true
```
### Maven
```
$ mvn spring-boot:run -Ddebug=true
```
